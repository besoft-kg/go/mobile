import React from 'react';
import { StackActions } from '@react-navigation/native';

export const navigationRef = React.createRef();

const navigate = (name, params) => {
  navigationRef.current?.navigate(name, params);
};

const push = (...args) => {
  navigationRef.current?.dispatch(StackActions.push(...args));
};

const replace = (...args) => {
  navigationRef.current?.dispatch(StackActions.replace(...args));
};

export default {
  navigate,
  push,
  replace,
}

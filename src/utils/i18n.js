import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import {DEBUG_MODE} from "./settings";
import AsyncStoragePlugin from 'i18next-react-native-async-storage'

import translationKy from '../locales/ky.json';
import translationRu from '../locales/ru.json';
import storage from "./storage";

const detectUserLanguage = (callback) => {
  return storage.get('language', 'ru').then(lng => callback(lng));
};

i18n
  .use(AsyncStoragePlugin(detectUserLanguage))
  .use(initReactI18next)
  .init({
    fallbackLng: ['ru', 'ky'],
    debug: DEBUG_MODE,
    defaultNS: 'common',
    ns: [
      'common',
      'home',
      'profile',
      'settings',
      'welcome',
      'add_trip',
      'history',
      'find_driver',
      'trips',
      'requests',
    ],
    resources: {
      ky: translationKy,
      ru: translationRu,
    },
    load: 'all',
    interpolation: {
      escapeValue: false,
    },
    react: {
      wait: true,
      useSuspense: true,
    },
  });

export default i18n;

import axios from 'axios';
import CustomError from './CustomError';
import {getCustomError} from './index';

export const APP_NAME = 'Besoft Go';

export const DEBUG_MODE = process.env.NODE_ENV !== "production";
export const API_VERSION = 1;
export const APP_VERSION_CODE = 11;
export const APP_VERSION_NAME = '0.1.1';
export const API_URL = DEBUG_MODE ? 'http://192.168.43.182:5000' : 'https://api.go.besoft.kg';
export const API_URL_WITH_VERSION = API_URL + '/v' + API_VERSION;

export const axios_instance = axios.create({
  baseURL: API_URL_WITH_VERSION,
  responseType: 'json',
  responseEncoding: 'utf8',
});

axios_instance._configs = {
  silence: false,
};

export function initialize(store){
  axios_instance.interceptors.request.use(function(config){
    if (store.token) config.headers.common.Authorization = 'Bearer ' + store.token;
    return config;
  }, function (error){
    if (!axios_instance._configs.silence) store.showError(error.message || error);
    if (error instanceof CustomError) throw error;
    throw new CustomError().setError(error);
  });

  axios_instance.interceptors.response.use(function (response){
    if (response.data.status < 0) {
      switch (response.data.result) {
        case 'token_is_expired':
          store.showInfo('Срок действия вашего токена истек, пожалуйста, войдите снова!');
          store.signOut();
          break;
        case 'token_is_invalid':
          store.showInfo('Ваш токен недействителен, пожалуйста, войдите снова!');
          store.signOut();
          break;
      }

      if (!axios_instance._configs.silence) {
        let message = null;

        switch (response.data.result) {
          case 'invalid_params':
            message = 'Неверные параметры!';
            break;

          default:
            break;
        }
        if (message) store.showError(message);
      }

      return Promise.reject(new CustomError().setResponse(response));
    }
    return response;
  }, function (error){
    error = getCustomError(error);
    if (!axios_instance._configs.silence) store.showError(error.description || error.message);

    switch (error.result) {
      case 'unauthorized':
        store.signOut();
        break;
    }

    return Promise.reject(error);
  });
}

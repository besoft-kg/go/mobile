import {AsyncStorage} from 'react-native';

const PREFIX = 'BesoftGo_Store';

const get = async (name, default_value = null) => {
  try {
    const value = JSON.parse(await AsyncStorage.getItem(`@${PREFIX}:${name}`));
    return value || default_value;
  } catch (error) {
    console.log(error);
    return default_value;
  }
};

const set = async (name, value) => {
  try {
    value = JSON.stringify(value);
    await AsyncStorage.setItem(`@${PREFIX}:${name}`, value);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

const remove = async (name) => {
  try {
    await AsyncStorage.removeItem(`@${PREFIX}:${name}`);
    return true;
  } catch (error) {
    console.log(error);
    return false;
  }
};

export default {
  get, set, remove,
};

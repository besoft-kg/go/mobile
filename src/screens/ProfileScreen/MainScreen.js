import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import {Headline, Avatar, Card, Text, Colors} from 'react-native-paper';
import {View} from 'react-native';
import ScreenWrapper from '../../ui/ScreenWrapper';
import {size} from '../../utils';
import {withTranslation} from 'react-i18next';

@withTranslation('profile')
@inject('store') @observer
class MainScreen extends BaseComponent {
  render() {
    const {user} = this.store;
    const {t} = this.props;

    return (
      <ScreenWrapper padding>
        <View flexDirection={'column'} alignItems={'center'} style={{marginVertical: size(12)}} justifyContent={'center'}>
          <Avatar.Text size={size(40)} label={user.two_letter_name} />
          <View style={{marginTop: size(7)}} flexDirection={'row'} alignItems={'center'}>
            <Headline style={{
              color: Colors.grey700,
            }}>{user.full_name}</Headline>
          </View>
        </View>
        <Card elevation={2}>
          <Card.Content>
            <Text style={{color: Colors.grey900}}><Text style={{color: Colors.grey600}}>{t('label.phone_number')}</Text> +{user.phone_number}</Text>
            <Text style={{color: Colors.grey900}}><Text style={{color: Colors.grey600}}>{t('label.full_name')}</Text> {user.full_name}</Text>
            <Text style={{color: Colors.grey900}}><Text style={{color: Colors.grey600}}>{t('label.gender')}</Text> {t(`user.gender.${user.gender}`)}</Text>
          </Card.Content>
        </Card>
      </ScreenWrapper>
    );
  }
}

export default MainScreen;

import React from 'react';
import BaseComponent from '../../components/BaseComponent';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreen from './MainScreen';
import {Colors, withTheme} from 'react-native-paper';
import {withTranslation} from 'react-i18next';
import {HiddenItem, OverflowMenu} from 'react-navigation-header-buttons';
import {MaterialHeaderButtons} from '../../ui/HeaderButtons';
import {inject, observer} from 'mobx-react';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const Stack = createStackNavigator();

@withTranslation('profile') @withTheme
@inject('store') @observer
class ProfileScreen extends BaseComponent {
  render() {
    const {t} = this.props;

    return (
      <Stack.Navigator screenOptions={{
        headerTintColor: '#fff',
        title: t('title'),
        headerStyle: {
          backgroundColor: Colors.blue700,
        },
      }} initialRouteName={'Main'}>
        <Stack.Screen options={{
          headerRight: () => (
            <MaterialHeaderButtons>
              <OverflowMenu
                style={{ marginHorizontal: 10 }}
                OverflowIcon={<MaterialIcons name="more-vert" size={23} color="#fff" />}
              >
                <HiddenItem onPress={() => this.store.signOut()} title={t('menu.logout')}/>
              </OverflowMenu>
            </MaterialHeaderButtons>
          )
        }} name={'Main'} component={MainScreen} />
      </Stack.Navigator>
    );
  }
}

export default ProfileScreen;

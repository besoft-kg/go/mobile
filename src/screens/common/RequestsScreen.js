import React from 'react';
import {inject, Observer, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import {
  Card,
  Colors,
  Subheading,
  Text,
  Title,
  withTheme,
} from 'react-native-paper';
import {FlatList, View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import TripItemComponent from '../../components/TripItemComponent';
import moment from 'moment';
import RequestButton from '../../ui/RequestButton';
import {size} from '../../utils';
import {withTranslation} from 'react-i18next';

const Tab = createMaterialTopTabNavigator();

@withTranslation('requests')
@withTheme
@inject('store') @observer
class RequestsScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.item = this[this.store.user.type === 'driver' ? 'offerStore' : 'tripStore'].items.get(props.route.params.item_id);
  }

  listRenderer = (data) => {
    return (
      <FlatList
        style={{marginTop: size(4)}}
        data={data}
        renderItem={({item}) => this.store.user.type === 'driver' ? (
          <Observer>
            {() => {
              const trip = this.tripStore.items.get(item.trip_id);
              const {t} = this.props;
              return (
                <Card style={{marginHorizontal: size(4), marginBottom: size(4)}}>
                  <Card.Content>
                    <Subheading>{trip.user.full_name}</Subheading>
                    <Text style={{color: Colors.grey600}}>{t('trip_datetime', {datetime: moment(trip.datetime).calendar()})}</Text>
                    <Text style={{color: Colors.grey600}}>{trip.persons_text}</Text>
                    {trip.note ? <Text>{trip.note}</Text> : null}
                    <Title style={{color: this.props.theme.colors.primary, fontWeight: 'bold'}}>{trip.price} сом</Title>
                    <RequestButton gItem={this.item} item={trip} userType={this.store.user.type}/>
                  </Card.Content>
                </Card>
              );
            }}
          </Observer>
        ) : (
          <Observer>
            {() => {
              const offer = this.offerStore.items.get(item.offer_id);
              const {t} = this.props;
              return (
                <Card style={{marginHorizontal: size(4), marginBottom: size(4)}}>
                  <Card.Content>
                    <Subheading>{offer.user.full_name}</Subheading>
                    <Text style={{color: Colors.grey600}}>{t('offer_datetime', {datetime: moment(offer.datetime).calendar()})}</Text>
                    {offer.note ? <Text>{offer.note}</Text> : null}
                    <RequestButton gItem={this.item} item={offer} userType={this.store.user.type}/>
                  </Card.Content>
                </Card>
              );
            }}
          </Observer>
        )}
      />
    );
  };

  render() {
    const requests = {
      all: this.item.requests.filter(v => v[`${this.store.user.type}_id`] === this.store.user.id),
      new: this.item.requests.filter(v => v[`${this.store.user.type}_id`] === this.store.user.id && v.status === 'pending' && v.sender !== this.store.user.type),
    };

    const {t} = this.props;

    return (
      <View style={{marginTop: size(4), flex: 1}}>
        <TripItemComponent item={this.item}/>
        <Tab.Navigator backBehavior={'none'} swipeEnabled={true} initialRouteName={'New'}>
          <Tab.Screen name={'New'} options={{title: t('tab.new', {count: requests.new.length})}}>
            {() => this.listRenderer(requests.new)}
          </Tab.Screen>
          <Tab.Screen name={'All'} options={{title: t('tab.all', {count: requests.all.length})}}>
            {() => this.listRenderer(requests.all)}
          </Tab.Screen>
        </Tab.Navigator>
      </View>
    );
  }
}

export default RequestsScreen;

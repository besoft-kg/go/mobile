import React from 'react';
import {inject, observer, Observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import moment from 'moment';
import {Card, Colors, Subheading, Text, Title, withTheme} from 'react-native-paper';
import {FlatList} from 'react-native';
import RequestButton from '../../ui/RequestButton';
import {withTranslation} from 'react-i18next';

@withTranslation('trips')
@withTheme @inject('store') @observer
class TripsScreen extends BaseComponent {
  constructor(props) {
    super(props);

    const {item_id} = this.props.route.params;

    this.item = this.offerStore.items.get(item_id);
  }

  componentWillUnmount() {
    this.item = null;
  }

  render() {
    const {t} = this.props;

    return (
      <FlatList
        style={{marginTop: 8}}
        data={this.item.trips.filter(v => v.request && v.request.status === 'accepted')}
        ListHeaderComponent={(
          <>
            <Subheading
              style={{color: Colors.grey600, margin: 8, fontWeight: 'bold'}}
            >{t('trips_screen.list_header_label', {place_title: this.item.to.title})}</Subheading>
            <Text style={{color: Colors.grey600, marginHorizontal: 8, marginBottom: 8}}>{t('trips_screen.list_header_description')}</Text>
          </>
        )}
        ListEmptyComponent={(
          <Card style={{marginHorizontal: 8}}>
            <Card.Content>
              <Subheading>{t('trips_screen.list_empty')}</Subheading>
            </Card.Content>
          </Card>
        )}
        renderItem={({item: trip}) => (
          <Observer>
            {() => (
              <Card style={{marginHorizontal: 8, marginBottom: 8}}>
                <Card.Content>
                  <Subheading>{trip.user.full_name}</Subheading>
                  <Text style={{color: Colors.grey600}}>{t('item.datetime', {datetime: moment(trip.datetime).calendar()})}</Text>
                  <Text style={{color: Colors.grey600}}>{trip.persons_text}</Text>
                  {trip.note ? <Text>{trip.note}</Text> : null}
                  <Title style={{color: this.props.theme.colors.primary, fontWeight: 'bold'}}>{trip.price} сом</Title>
                  <RequestButton gItem={this.item} item={trip} userType={this.store.user.type} />
                </Card.Content>
              </Card>
            )}
          </Observer>
        )}
      />
    );
  }
}

export default TripsScreen;

import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import moment from 'moment';
import {Card, Colors, List, Subheading, Text, withTheme} from 'react-native-paper';
import {FlatList, Image} from 'react-native';
import TripItemComponent from '../../components/TripItemComponent';
import {Observer} from 'mobx-react';
import RequestButton from '../../ui/RequestButton';
import {size} from '../../utils';
import tripImage from '../../assets/trip.png';
import ScreenWrapper from '../../ui/ScreenWrapper';
import {withTranslation} from 'react-i18next';

@withTranslation('trips')
@withTheme
@inject('store') @observer
class TripScreen extends BaseComponent {
  constructor(props) {
    super(props);

    const {item_id} = this.props.route.params;

    this.props.navigation.setOptions({
      title: `Предложение #${item_id}`,
    });

    this.item = this.tripStore.items.get(item_id);
  }

  componentWillUnmount() {
    this.item = null;
  }

  render() {
    const {t} = this.props;

    if (!this.item) {
      return (
        <ScreenWrapper padding>
          <Card elevation={3}>
            <Card.Content alignItems={'center'}>
              <Image source={tripImage} style={{marginVertical: size(7), height: size(30), width: size(30)}}
                     resizeMode={'contain'}/>
              <Text style={{textAlign: 'center', color: Colors.grey500}}>{t('item.not_found')}</Text>
            </Card.Content>
          </Card>
        </ScreenWrapper>
      );
    }

    const new_requests = this.item.requests.filter(v => v.status === 'pending' && v.sender !== this.store.user.type);

    return (
      <FlatList
        style={{marginTop: size(4)}}
        data={(this.item.request && this.item.request.status === 'accepted') || this.item.is_past ? [] : this.item.offers.filter(v => !v.request || v.request.status === 'pending')}
        ListHeaderComponent={(
          <>
            <TripItemComponent item={this.item}/>
            {(this.item.request && this.item.request.status === 'accepted') ? (
              <Card style={{marginHorizontal: size(4), marginBottom: size(4)}}>
                <Card.Content>
                  <Subheading>{t('your_driver', {full_name: this.item.request.driver.full_name})}</Subheading>
                  <RequestButton userType={this.store.user.type}
                                 gItem={this.offerStore.items.get(this.item.request.offer_id)} item={this.item}/>
                </Card.Content>
              </Card>
            ) : (
              <Card style={{marginHorizontal: 8}}>
                <List.Item
                  titleStyle={new_requests.length > 0 ? {color: Colors.white, fontWeight: 'bold'} : {}}
                  style={new_requests.length > 0 ? {backgroundColor: Colors.blue700} : {}}
                  title={`Запросы${new_requests.length > 0 ? ` (+${new_requests.length})` : ''}`}
                  onPress={() => this.props.navigation.navigate('Requests', {item_id: this.item.id})}
                  left={props => <List.Icon {...props} color={new_requests.length > 0 ? Colors.white : null}
                                            icon="help-outline"/>}
                  right={props => <List.Icon {...props} color={new_requests.length > 0 ? Colors.white : null}
                                             icon="keyboard-arrow-right"/>}
                />
              </Card>
            )}
            {(this.item.request && this.item.request.status === 'accepted') || this.item.is_past ? null : (
              <>
                <Subheading style={{color: Colors.grey600, margin: size(4), fontWeight: 'bold'}}
                >{t('list_header_label', {place_title: this.item.to.title})}</Subheading>
                <Text style={{color: Colors.grey600, marginHorizontal: size(4), marginBottom: size(4)}}>{t('list_header_description')}</Text>
              </>
            )}
          </>
        )}
        ListEmptyComponent={(this.item.request && this.item.request.status === 'accepted') || this.item.is_past ? null : (
          <Card style={{marginHorizontal: size(4)}}>
            <Card.Content alignItems={'center'}>
              <Image source={tripImage} style={{marginVertical: size(7), height: size(30), width: size(30)}}
                     resizeMode={'contain'}/>
              <Text style={{textAlign: 'center', color: Colors.grey500}}>{t('list_empty')}</Text>
            </Card.Content>
          </Card>
        )}
        renderItem={({item: offer}) => (
          <Observer>
            {() => (
              <Card style={{marginHorizontal: size(4), marginBottom: size(4)}}>
                <Card.Content>
                  <Subheading>{offer.user.full_name}</Subheading>
                  <Text style={{color: Colors.grey600}}>{t('item.datetime', {datetime: moment(offer.datetime).calendar()})}</Text>
                  {offer.note ? <Text>{offer.note}</Text> : null}
                  <RequestButton gItem={this.item} item={offer} userType={this.store.user.type}/>
                </Card.Content>
              </Card>
            )}
          </Observer>
        )}
      />
    );
  }
}

export default TripScreen;

import React from 'react';
import {inject, Observer, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import {ActivityIndicator, Card, Colors, Divider, List, Subheading, Text, Title, withTheme} from 'react-native-paper';
import {FlatList, Image, View} from 'react-native';
import moment from 'moment';
import RequestButton from '../../ui/RequestButton';
import {size} from '../../utils';
import tripImage from '../../assets/trip.png';
import ScreenWrapper from '../../ui/ScreenWrapper';
import {withTranslation} from 'react-i18next';

@withTranslation('offers')
@withTheme
@inject('store') @observer
class OfferScreen extends BaseComponent {
  constructor(props) {
    super(props);

    this.props.navigation.setOptions({
      title: `Предложение #${this.props.route.params.item_id}`,
    });
  }

  componentDidMount(): void {
    if (!this.offerStore.items.has(this.props.route.params.item_id)) {
      this.offerStore.fetchItem(this.props.route.params.item_id);
    }
  }

  render() {
    if (this.offerStore.is_fetching) return (
      <View flex={1} justifyContent={'center'}>
        <ActivityIndicator/>
      </View>
    );

    const item = this.offerStore.items.get(this.props.route.params.item_id);
    const {t} = this.props;

    if (!item) {
      return (
        <ScreenWrapper padding>
          <Card elevation={3}>
            <Card.Content alignItems={'center'}>
              <Image source={tripImage} style={{marginVertical: size(7), height: size(30), width: size(30)}}
                     resizeMode={'contain'}/>
              <Text style={{textAlign: 'center', color: Colors.grey500}}>{t('item.not_found')}</Text>
            </Card.Content>
          </Card>
        </ScreenWrapper>
      );
    }

    return (
      <FlatList
        style={{marginTop: size(4)}}
        data={item.is_past ? [] : item.trips.filter(v => !v.request || v.request.status === 'pending')}
        ListHeaderComponent={(
          <>
            <Card style={{marginHorizontal: size(4)}}>
              <List.Item
                title={t('list.requests')}
                onPress={() => this.props.navigation.navigate('Requests', {item_id: item.id})}
                left={props => <List.Icon {...props} icon="help-outline"/>}
                right={props => <List.Icon {...props} icon="keyboard-arrow-right"/>}
              />
              <Divider/>
              <List.Item
                title={t('list.passengers')}
                onPress={() => this.props.navigation.navigate('Trips', {item_id: item.id})}
                left={props => <List.Icon {...props} icon="group"/>}
                right={props => <List.Icon {...props} icon="keyboard-arrow-right"/>}
              />
            </Card>
            {!item.is_past && (
              <>
                <Subheading
                  style={{color: Colors.grey600, margin: size(4), fontWeight: 'bold'}}
                >{t('list_header_label', {place_title: item.to.title})}</Subheading>
                <Text style={{color: Colors.grey600, marginHorizontal: size(4), marginBottom: size(4)}}>{t('list_header_description')}</Text>
              </>
            )}
          </>
        )}
        ListEmptyComponent={!item.is_past && (
          <Card style={{marginHorizontal: size(4)}}>
            <Card.Content alignItems={'center'}>
              <Image source={tripImage} style={{marginVertical: size(7), height: size(30), width: size(30)}}
                     resizeMode={'contain'}/>
              <Text style={{textAlign: 'center', color: Colors.grey500}}>{t('list_empty')}</Text>
            </Card.Content>
          </Card>
        )}
        renderItem={({item: trip}) => (
          <Observer>
            {() => (
              <Card style={{marginHorizontal: size(4), marginBottom: size(4)}}>
                <Card.Content>
                  <Subheading>{trip.user.full_name}</Subheading>
                  <Text style={{color: Colors.grey600}}>{t('item.datetime', {datetime: moment(trip.datetime).calendar()})}</Text>
                  <Text style={{color: Colors.grey600}}>{trip.persons_text}</Text>
                  {trip.note ? <Text>{trip.note}</Text> : null}
                  <Title style={{color: this.props.theme.colors.primary, fontWeight: 'bold'}}>{trip.price} сом</Title>
                  <RequestButton gItem={item} item={trip} userType={this.store.user.type}/>
                </Card.Content>
              </Card>
            )}
          </Observer>
        )}
      />
    );
  }
}

export default OfferScreen;

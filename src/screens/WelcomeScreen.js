import React from 'react';
import {View, StatusBar} from 'react-native';
import {inject, observer} from 'mobx-react';
import BaseComponent from '../components/BaseComponent';
import ViewPager from '@react-native-community/viewpager';
import {action, observable} from 'mobx';
import LanguagePageComponent from '../components/welcome/LanguagePageComponent';
import AuthPageComponent from '../components/welcome/AuthPageComponent';
import RegisterPageComponent from '../components/welcome/RegisterPageComponent';
import LoginPageComponent from '../components/welcome/LoginPageComponent';

@inject('store') @observer
class WelcomeScreen extends BaseComponent {
  view_pager_ref = React.createRef();
  phone_number_ref = React.createRef();
  verification_code_ref = React.createRef();

  @observable phone_number = '';
  @observable status = '';

  constructor(props) {
    super(props);

    this.initial_page = 0;

    if (this.store.language) {
      this.initial_page++;
    }
  }

  @action setValue = (name, value) => this[name] = value;

  render() {
    return (
      <>
        <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>
        <ViewPager
          onPageSelected={e => {
            if (e.nativeEvent.position === 3) {
              this.phone_number_ref.focus();
            } else if (e.nativeEvent.position === 4) {
              this.verification_code_ref.focus();
            }
          }}
          ref={this.view_pager_ref}
          scrollEnabled={this.store.language !== null && this.status === ''}
          flex={1}
          initialPage={this.initial_page}>
          <View>
            <LanguagePageComponent viewPagerRef={this.view_pager_ref}/>
          </View>
          <View>
            <AuthPageComponent
              status={this.status}
              ref={r => this.phone_number_ref = r}
              viewPagerRef={this.view_pager_ref}
              onChange={(name, value) => this.setValue(name, value)}
            />
          </View>
          {this.status !== '' && (
            <View>
              {this.status === 'not_exists' ? (
                <RegisterPageComponent
                  ref={r => this.verification_code_ref = r}
                  status={this.status}
                  phoneNumber={this.phone_number}
                />
              ) : (
                <LoginPageComponent
                  ref={r => this.verification_code_ref = r}
                  status={this.status}
                  phoneNumber={this.phone_number}
                />
              )}
            </View>
          )}
        </ViewPager>
      </>
    );
  }
}

export default WelcomeScreen;

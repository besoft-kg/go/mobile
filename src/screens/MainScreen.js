import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {AppState, SafeAreaView, StatusBar} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import HomeScreen from './HomeScreen';
import {Colors, withTheme} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import HistoryScreen from './HistoryScreen';
import ProfileScreen from './ProfileScreen';
import {action, observable} from 'mobx';
// import messaging from '@react-native-firebase/messaging';
// import RootNavigator from '../utils/RootNavigator';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {withTranslation} from 'react-i18next';
import AddTripScreen from './AddTripScreen';
import {requester} from '../utils';

const Tab = createMaterialBottomTabNavigator();

@withTranslation()
@withTheme
@inject('store')
@observer
class MainScreen extends BaseComponent {
  @observable statusbar_bg_color = null;
  @observable app_state = AppState.currentState;

  constructor(props) {
    super(props);

    this.statusbar_bg_color = this.props.theme.colors.primary;
  }

  @action setStatusBarBgColor(c) {
    this.statusbar_bg_color = c;
  }

  @action _handleAppStateChange = async (nextAppState) => {
    if (this.app_state && this.app_state.match(/inactive|background/) && nextAppState === 'active') {
      console.log('foreground');
    }
    console.log(nextAppState);
    this.app_state = nextAppState;
  };

  componentWillUnmount() {
    //this.unsubscribe();
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  componentDidMount() {
    // messaging().setBackgroundMessageHandler(async remoteMessage => {
    //   console.log('Message handled in the background!', remoteMessage);
    //
    //   switch (remoteMessage.data.type) {
    //     case 'driver_appeared':
    //     case 'passenger_appeared':
    //       this.fetchVoyage();
    //       break;
    //   }
    // });

    // this.unsubscribe = messaging().onMessage(async remoteMessage => {
    //   console.log(remoteMessage);
    //
    //   switch (remoteMessage.data.type) {
    //     case 'driver_appeared':
    //     case 'passenger_appeared':
    //       this.fetchVoyage();
    //       break;
    //   }
    // });

    // messaging().onNotificationOpenedApp(remoteMessage => {
    //   console.log('Notification caused app to open from background state:', remoteMessage);
    //
    //   this.notificationStore.setValue('unread_items_count', Number(remoteMessage.data._unread_notifications_count));
    //
    //   switch (remoteMessage.data.type) {
    //     case 'requests.created':
    //       if (this.store.user.type === 'passenger') {
    //         RootNavigator.navigate('Home', {
    //           screen: 'Trip',
    //           params: {item_id: remoteMessage.data.trip_id},
    //         });
    //       } else {
    //         RootNavigator.navigate('Home', {
    //           screen: 'Offer',
    //           params: {item_id: remoteMessage.data.offer_id},
    //         });
    //       }
    //       break;
    //   }
    // });

    // messaging().getInitialNotification().then(remoteMessage => {
    //   if (remoteMessage) {
    //     console.log(
    //       'Notification caused app to open from quit state:',
    //       remoteMessage.notification,
    //     );
    //   }
    // });

    // messaging().getToken().then(fcm_token => this.store.setFcmToken(fcm_token));
    // messaging().onTokenRefresh(fcm_token => this.store.setFcmToken(fcm_token));

    AppState.addEventListener('change', this._handleAppStateChange);
  }

  render() {
    const {t} = this.props;

    return (
      <>
        <StatusBar barStyle="light-content" backgroundColor={this.statusbar_bg_color}/>
        <Tab.Navigator shifting={true} initialRouteName={'Home'}>
          <Tab.Screen
            listeners={{tabPress: () => this.setStatusBarBgColor(this.props.theme.colors.primary)}}
            name="Home"
            options={{
              title: t('home'),
              tabBarColor: this.props.theme.colors.primary,
              tabBarIcon: ({color}) => <Icon color={color} size={24} name={'home'}/>,
            }}
            component={HomeScreen}/>
          <Tab.Screen
            listeners={{tabPress: () => this.setStatusBarBgColor(Colors.teal700)}}
            name="AddTrip"
            options={{
              title: t('add_trip'),
              tabBarColor: Colors.teal700,
              tabBarIcon: ({color}) => <Icon color={color} size={24} name={'add-circle'}/>,
            }} component={AddTripScreen}/>
          <Tab.Screen
            listeners={{tabPress: () => this.setStatusBarBgColor(Colors.cyan700)}}
            name="History"
            options={{
              title: t('history'),
              tabBarColor: Colors.cyan700,
              tabBarIcon: ({color}) => <Icon color={color} size={24} name={'history'}/>,
            }} component={HistoryScreen}/>
          <Tab.Screen
            listeners={{tabPress: () => this.setStatusBarBgColor(Colors.blue700)}}
            name="Profile"
            options={{
              title: t('profile'),
              tabBarColor: Colors.blue700,
              tabBarIcon: ({color}) => <Icon color={color} size={24} name={'person'}/>,
            }} component={ProfileScreen}/>
        </Tab.Navigator>
      </>
    );
  }
}

export default MainScreen;

import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import {FlatList} from 'react-native';
import {action, observable} from 'mobx';
import {requester, size} from '../../utils';
import TripItemComponent from '../../components/TripItemComponent';
import moment from 'moment';
import {Card, Colors, Subheading, Text} from 'react-native-paper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {withTranslation} from 'react-i18next';

@withTranslation('history')
@inject('store') @observer
class MainScreen extends BaseComponent {
  @observable loading = false;
  @observable items = [];

  componentDidMount(): void {
    this.fetch();
  }

  @action fetch = () => {
    if (this.loading) {
      return;
    }
    this.loading = true;
    requester.get(`/trip/history`).then(({data}) => {
      this.tripStore.setItems(data.payload.data);
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      this.loading = false;
    });
  };

  render() {
    const loading = this.tripStore.is_fetching || this.loading;
    const {t} = this.props;

    return (
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        refreshing={loading}
        onRefresh={() => this.fetch()}
        renderItem={({item, index}) => (
          <TripItemComponent item={item} key={index}/>
        )}
        style={{paddingTop: size(4)}}
        data={Array.from(this.tripStore.items.values()).sort((a, b) => a.datetime - b.datetime).filter(v => v.datetime <= moment().subtract(5, 'hours'))}
        ListFooterComponent={(
          <Text style={{textAlign: 'center', color: Colors.grey600, marginBottom: size(10)}}>{t('seen_last_30_items')}</Text>
        )}
        ListEmptyComponent={(
          <Card elevation={2} style={{margin: size(4), marginTop: 0}}>
            <Card.Content alignItems={'center'}>
              <MaterialIcons name={'history'} style={{marginVertical: size(8)}} color={Colors.grey300}
                             size={size(30)}/>
              <Subheading style={{color: Colors.grey500}}>{t('list_is_empty')}</Subheading>
            </Card.Content>
          </Card>
        )}/>
    );
  }
}

export default MainScreen;

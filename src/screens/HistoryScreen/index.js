import React from 'react';
import BaseComponent from '../../components/BaseComponent';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreen from './MainScreen';
import {Colors} from 'react-native-paper';
import OfferScreen from '../common/OfferScreen';
import TripScreen from '../common/TripScreen';
import RequestsScreen from '../common/RequestsScreen';
import TripsScreen from '../common/TripsScreen';
import {withTranslation} from 'react-i18next';

const Stack = createStackNavigator();

@withTranslation('history')
class HistoryScreen extends BaseComponent {
  render() {
    const {t} = this.props;

    return (
      <Stack.Navigator screenOptions={{
        headerTintColor: '#fff',
        animationTypeForReplace: 'pop',
        title: t('title'),
        headerStyle: {
          backgroundColor: Colors.cyan700,
        },
      }} initialRouteName={'Main'}>
        <Stack.Screen name={'Main'} component={MainScreen}/>
        <Stack.Screen name={'Offer'} component={OfferScreen} />
        <Stack.Screen name={'Trip'} component={TripScreen} />
        <Stack.Screen options={{title: t('requests')}} name={'Requests'} component={RequestsScreen} />
        <Stack.Screen options={{title: t('passengers')}} name={'Trips'} component={TripsScreen} />
      </Stack.Navigator>
    );
  }
}

export default HistoryScreen;

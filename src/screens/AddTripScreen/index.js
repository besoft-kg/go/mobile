import React from 'react';
import BaseComponent from '../../components/BaseComponent';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreen from './MainScreen';
import {Colors} from 'react-native-paper';
import {withTranslation} from 'react-i18next';

const Stack = createStackNavigator();

@withTranslation('add_trip')
class AddTripScreen extends BaseComponent {
  render() {
    const {t} = this.props;

    return (
      <Stack.Navigator screenOptions={{
        headerTintColor: '#fff',
        animationTypeForReplace: 'pop',
        title: t('title'),
        headerStyle: {
          backgroundColor: Colors.teal700,
        },
      }} initialRouteName={'Main'}>
        <Stack.Screen name={'Main'} component={MainScreen} />
      </Stack.Navigator>
    );
  }
}

export default AddTripScreen;

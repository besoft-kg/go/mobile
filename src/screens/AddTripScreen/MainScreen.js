import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import {
  Button, Card, Colors, Dialog,
  Divider, List, Portal, Subheading,
  Text, TextInput,
} from 'react-native-paper';
import phoneNumberExamples from 'libphonenumber-js/examples.mobile.json';
import {getCustomError, requester, size} from '../../utils';
import {FlatList, TouchableOpacity, TouchableWithoutFeedback, View} from 'react-native';
import {withTranslation} from 'react-i18next';
import moment from 'moment';
import DateTimePicker from '@react-native-community/datetimepicker';
import {action, observable} from 'mobx';
import ScreenWrapper from '../../ui/ScreenWrapper';
import {getExampleNumber} from 'libphonenumber-js';
import RadioButtons from '../../ui/RadioButtons';

@withTranslation('add_trip')
@inject('store') @observer
class MainScreen extends BaseComponent {
  @observable select_dialog = null;
  @observable sub_places = false;
  @observable places = [];
  @observable persons_modal = false;
  @observable datetime_modal = false;
  @observable datetime_mode = 'date';
  @observable loading = false;
  @observable contact_modal = false;
  @observable contact_index = -1;

  @observable from = null;
  @observable to = null;
  @observable persons = '3';
  @observable note = null;
  @observable price = null;
  @observable datetime = moment().add(1, 'day').hours(10).minutes(0).seconds(0);
  @observable contacts = [];

  constructor(props) {
    super(props);

    this.contacts = [{
      value: this.store.user.phone_number,
      type: 'phone_number',
      data: {
        telegram: false,
        whatsapp: false,
      },
    }];

    this.phone_number_placeholder = getExampleNumber('KG', phoneNumberExamples).formatNational();
  }

  @action clear = () => {
    this.from = null;
    this.to = null;
    this.note = null;
    this.price = null;
    this.datetime = moment().add(1, 'day').hours(10).minutes(0).seconds(0);

    this.select_dialog = null;
    this.sub_places = false;
    this.persons_modal = false;
    this.datetime_modal = false;
    this.datetime_mode = 'date';
    this.loading = false;
  };

  @action select = (direction) => {
    this.select_dialog = direction;
    this.sub_places = false;
    this.places = this.placeStore.items.filter(v => v.parent_id === null);
  };

  @action selectPlace = (place, index) => {
    if (!this.select_dialog) {
      return;
    }
    const children = this.placeStore.items.filter(v => v.parent_id === place.id);
    if (children.length === 0 || (this.sub_places && index === 0)) {
      this[this.select_dialog] = place;
      this.select_dialog = null;
    } else {
      this.places = [place, ...children.filter(v => {
        if (this.select_dialog === 'from' && this.to) {
          return v.id !== this.to.id;
        }
        if (this.select_dialog === 'to' && this.from) {
          return v.id !== this.from.id;
        }
        return true;
      })];
      this.sub_places = true;
    }
  };

  @action selectPersons = (count) => {
    this.persons = count;
    this.persons_modal = false;
  };

  @action setValue = (name, value) => this[name] = value;

  @action togglePersonsModal = (s) => this.persons_modal = s;
  @action toggleDatetimeModal = (s) => this.datetime_modal = s;
  @action toggleContactModal = (s, index) => {
    this.contact_modal = s;
    this.contact_index = index;
  };

  @action setDatetime = (e, date) => {
    if (e.type !== 'dismissed') {
      this.datetime = moment(date);
    } else {
      this.datetime_modal = false;
      this.datetime_mode = 'date';
      return;
    }
    if (this.datetime_mode === 'time') {
      this.datetime_modal = false;
      this.datetime_mode = 'date';
    } else {
      this.datetime_mode = 'time';
    }
  };

  @action add = () => {
    if (this.loading || !this.from || !this.to) {
      return;
    }
    this.loading = true;
    requester.post('/trip', {
      from: this.from.id,
      to: this.to.id,
      persons: this.persons,
      datetime: this.datetime.unix(),
      note: this.note,
      price: this.price,
      who: this.store.global_who,
    }).then(({data}) => {
      if (data.result === 'success') {
        this.tripStore.setItem(data.payload);
        this.props.navigation.goBack();
        this.store.showSuccess('Успешно добавлено!');
        this.clear();
      }
    }).catch(e => {
      e = getCustomError(e);
      console.log(e.toSnapshot());
    }).finally(() => {
      this.loading = false;
    });
  };

  render() {
    const {t} = this.props;
    const contact = this.contact_index >= 0 ? this.contacts[this.contact_index] : {
      value: '',
      type: 'phone_number',
      data: {telegram: false, whatsapp: false},
    };

    return (
      <>
        {this.datetime_modal && (
          <DateTimePicker
            value={this.datetime.toDate()}
            mode={this.datetime_mode}
            is24Hour={true}
            display="default"
            onChange={(e, date) => this.setDatetime(e, date)}
            minimumDate={moment().add(1, 'hour').minutes(0).seconds(0).toDate()}
            maximumDate={moment().add(7, 'day').hours(0).minutes(0).seconds(0).toDate()}
          />
        )}

        <Portal>
          <Dialog
            visible={this.select_dialog !== null}
            onDismiss={() => this.select_dialog = null}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.selectPlace(item, index)} style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item.title}</Text>
                  </TouchableOpacity>
                )}
                data={this.places}/>
            </Dialog.Content>
          </Dialog>
          <Dialog
            visible={this.persons_modal}
            onDismiss={() => this.togglePersonsModal(false)}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.selectPersons((index + 1).toString())}
                                    style={{paddingVertical: size(4)}} key={index}>
                    <Text>{item}</Text>
                  </TouchableOpacity>
                )}
                data={['1', '2', '3', '4', '5', '6', '7', '8', '9']}/>
            </Dialog.Content>
          </Dialog>
          <Dialog
            visible={this.contact_modal}
            onDismiss={() => this.toggleContactModal(false)}>
            <Dialog.Content>
              <TextInput
                label={t('phone_number_input_label')}
                autoCompleteType={'tel'}
                value={contact.value}
                keyboardType={'phone-pad'}
                returnKeyType={'done'}
                mode={'outlined'}
                dense={true}
                dataDetectorTypes={'phoneNumber'}
                autoFocus={true}
                blurOnSubmit={true}
                textContentType={'telephoneNumber'}
                placeholder={this.phone_number_placeholder}
                onChangeText={text => this.setPhoneNumber(text)}
              />
            </Dialog.Content>
          </Dialog>
        </Portal>

        <ScreenWrapper scroll>
          <Card style={{margin: size(4)}} elevation={1}>
            <Card.Content>
              <TouchableWithoutFeedback onPress={() => this.select('from')}>
                <View style={{marginBottom: size(4)}}>
                  <TextInput
                    label={t('add_trip_from')}
                    editable={false}
                    value={this.from ? this.from[`title_${this.store.language}`] : ''}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.select('to')}>
                <View>
                  <TextInput
                    label={t('add_trip_to')}
                    editable={false}
                    value={this.to ? this.to[`title_${this.store.language}`] : ''}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
              <RadioButtons
                style={{marginTop: size(2)}}
                options={[
                  {value: 'driver', label: t(`i_am.driver`)},
                  {value: 'passenger', label: t(`i_am.passenger`)},
                ]}
                value={this.store.global_who}
                onChange={value => this.store.setValue('global_who', value)}/>
            </Card.Content>
          </Card>
          <Card style={{margin: size(4), marginTop: 0}} elevation={1}>
            <Card.Content>
              <TextInput
                autoCompleteType={'off'}
                style={{marginBottom: size(4)}}
                keyboardType={'numeric'}
                label={t('add_trip_price')}
                placeholder={'договорная'}
                value={this.price}
                mode={'outlined'}
                dense={true}
                returnKeyType={'next'}
                onChangeText={(text) => this.setValue('price', text)}
              />
              <TouchableWithoutFeedback onPress={() => this.togglePersonsModal(true)}>
                <View style={{marginBottom: size(4)}}>
                  <TextInput
                    label={t('add_trip_persons')}
                    editable={false}
                    value={this.persons}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.toggleDatetimeModal(true)}>
                <View>
                  <TextInput
                    label={t('add_trip_datetime')}
                    editable={false}
                    value={this.datetime.calendar()}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
            </Card.Content>
          </Card>
          <Card style={{margin: size(4), marginTop: 0}} elevation={1}>
            <Card.Content>
              <Subheading style={{fontWeight: 'bold', color: Colors.grey500}}>{t('contacts')}</Subheading>
              {this.contacts.map((v, i) => (
                <React.Fragment key={i}>
                  <List.Item
                    title={`+${v.value}`}
                    description={[v.data.whatsapp ? 'WhatsApp' : null, v.data.telegram ? 'Telegram' : null].filter(j => j).join(', ')}
                    // right={props => <TouchableOpacity onPress={() => this.toggleContactModal(true, i)}><List.Icon {...props} icon="edit"/></TouchableOpacity>}
                  />
                  {/*<Divider/>*/}
                </React.Fragment>
              ))}
              {/*<Button onPress={() => this.toggleContactModal(true, -1)} theme={{colors: {primary: Colors.teal700}}} icon={'add'} style={{marginTop: size(4)}}>Добавить*/}
              {/*  номер</Button>*/}
            </Card.Content>
          </Card>
          <Card style={{margin: size(4), marginTop: 0}} elevation={1}>
            <Card.Content>
              <TextInput
                label={t('add_trip_note')}
                placeholder={t('add_trip_note_placeholder')}
                value={this.note}
                onChangeText={s => this.setValue('note', s)}
                dense={true}
                numberOfLines={3}
                multiline={true}
              />
            </Card.Content>
          </Card>
          <Button
            loading={this.loading}
            mode={'contained'}
            onPress={() => this.add()}
            style={{marginHorizontal: size(4), backgroundColor: Colors.teal700, marginBottom: size(4)}}
          >{t('submit')}</Button>
        </ScreenWrapper>
      </>
    );
  }
}

export default MainScreen;

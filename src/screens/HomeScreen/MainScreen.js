import React from 'react';
import {inject, observer} from 'mobx-react';
import {FlatList, Image} from 'react-native';
import BaseComponent from '../../components/BaseComponent';
import {ActivityIndicator, Paragraph, Button, Card, Text, Colors, Subheading} from 'react-native-paper';
import TripItemComponent from '../../components/TripItemComponent';
import moment from 'moment';
import {size} from '../../utils';
import tripImage from '../../assets/trip.png';
import {withTranslation} from 'react-i18next';
import imageDriverMale from '../../assets/driver-male.png';
import imageDriverFemale from '../../assets/driver-female.png';
import imagePassengerMale from '../../assets/passenger-male.png';
import imagePassengerFemale from '../../assets/passenger-female.png';

@withTranslation('home')
@inject('store') @observer
class MainScreen extends BaseComponent {
  add = (who) => {
    this.store.setValue('global_who', who);
    this.props.navigation.navigate('AddTrip');
  };

  find = (who) => {
    this.props.navigation.navigate(`Find`, {who});
  };

  render() {
    const {t} = this.props;

    return (
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TripItemComponent item={item} key={index}/>
        )}
        data={Array.from(this.tripStore.items.values()).sort((a, b) => a.datetime - b.datetime).filter(v => v.user_id === this.store.user.id && v.datetime > moment().subtract(5, 'hours'))}
        ListHeaderComponent={(
          <>
            <Card style={{margin: size(4)}} elevation={2}>
              <Card.Content>
                <Image source={this.store.user.gender === 'male' ? imagePassengerMale : imagePassengerFemale} style={{alignSelf: 'center', marginVertical: size(7), height: size(30), width: size(50)}}
                       resizeMode={'contain'}/>
                <Subheading>{t('are_you_going.passenger')}</Subheading>
                <Paragraph style={{color: Colors.grey600}}>{t('list_header_description.passenger')}</Paragraph>
                <Button onPress={() => this.find('driver')} icon={'search'} mode={'outlined'} style={{marginTop: size(4)}}>{t(`find_driver`)}</Button>
                <Button onPress={() => this.add('passenger')} icon={'add'} mode={'outlined'} style={{marginTop: size(4)}}>{t(`add_trip`)}</Button>
              </Card.Content>
            </Card>
            <Card style={{margin: size(4), marginTop: 0}} elevation={2}>
              <Card.Content>
                <Image source={this.store.user.gender === 'male' ? imageDriverMale : imageDriverFemale} style={{alignSelf: 'center', marginVertical: size(7), height: size(30), width: size(50)}}
                       resizeMode={'contain'}/>
                <Subheading>{t('are_you_going.driver')}</Subheading>
                <Paragraph style={{color: Colors.grey600}}>{t('list_header_description.driver')}</Paragraph>
                <Button onPress={() => this.find('passenger')} icon={'search'} mode={'outlined'} style={{marginTop: size(4)}}>{t(`find_passenger`)}</Button>
                <Button onPress={() => this.add('driver')} icon={'add'} mode={'outlined'} style={{marginTop: size(4)}}>{t(`add_trip`)}</Button>
              </Card.Content>
            </Card>
            <Subheading style={{color: Colors.grey500, marginLeft: size(4), marginBottom: size(4), fontWeight: 'bold'}}>{t(`your_trips`)}</Subheading>
          </>
        )}
        ListEmptyComponent={(
          <Card elevation={2} style={{margin: size(4), marginTop: 0}}>
            <Card.Content alignItems={'center'}>
              {this.tripStore.is_fetching ? (
                <ActivityIndicator/>
              ) : (
                <>
                  <Image source={tripImage} style={{marginVertical: size(7), height: size(30), width: size(30)}}
                         resizeMode={'contain'}/>
                  <Text style={{textAlign: 'center', color: Colors.grey500}}>{t(`list_is_empty`)}</Text>
                </>
              )}
            </Card.Content>
          </Card>
        )}/>
    );
  }
}

export default MainScreen;

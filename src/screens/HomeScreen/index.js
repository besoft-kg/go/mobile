import React from 'react';
import BaseComponent from '../../components/BaseComponent';
import {createStackNavigator} from '@react-navigation/stack';
import MainScreen from './MainScreen';
import {withTheme} from 'react-native-paper';
import {APP_NAME} from '../../utils/settings';
import AddOfferScreen from './AddOfferScreen';
import {withTranslation} from 'react-i18next';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {MaterialHeaderButtons} from '../../ui/HeaderButtons';
import {HiddenItem, OverflowMenu} from 'react-navigation-header-buttons';
import AboutAppScreen from './AboutAppScreen';
import Find from './Find';

const Stack = createStackNavigator();

@withTheme @withTranslation('home')
class HomeScreen extends BaseComponent {
  render() {
    const {t} = this.props;

    return (
      <Stack.Navigator screenOptions={{
        headerTintColor: '#fff',
        animationTypeForReplace: 'pop',
        title: APP_NAME,
        headerStyle: {
          backgroundColor: this.props.theme.colors.primary,
        },
      }} initialRouteName={'Main'}>
        <Stack.Screen name={'Main'} options={{
          headerRight: () => (
            <MaterialHeaderButtons>
              <OverflowMenu
                style={{ marginHorizontal: 10 }}
                OverflowIcon={<MaterialIcons name="more-vert" size={23} color="#fff" />}
              >
                <HiddenItem onPress={() => this.props.navigation.navigate('AboutApp')} title={t('about_app')}/>
              </OverflowMenu>
            </MaterialHeaderButtons>
          )
        }} component={MainScreen} />
        <Stack.Screen options={{title: t('add_offer.title')}} name={'AddOffer'} component={AddOfferScreen} />
        <Stack.Screen options={{title: t('find')}} name={'Find'} component={Find} />

        <Stack.Screen options={{title: t('about_app')}} name={'AboutApp'} component={AboutAppScreen} />
      </Stack.Navigator>
    );
  }
}

export default HomeScreen;

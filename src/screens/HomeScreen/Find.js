import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import {
  ActivityIndicator,
  Button,
  Card, Colors,
  Dialog,
  Divider,
  Portal,
  Text,
  TextInput,
} from 'react-native-paper';
import {getCustomError, requester, size} from '../../utils';
import {FlatList, Image, TouchableOpacity, TouchableWithoutFeedback, View} from 'react-native';
import {withTranslation} from 'react-i18next';
import moment from 'moment';
import {action, observable} from 'mobx';
import TripItemComponent from '../../components/TripItemComponent';
import tripImage from '../../assets/trip.png';
import RadioButtons from '../../ui/RadioButtons';

@withTranslation('find')
@inject('store') @observer
class Find extends BaseComponent {
  @observable select_dialog = null;
  @observable sub_places = false;
  @observable places = [];
  @observable loading = false;

  @observable from = null;
  @observable to = null;
  @observable who = 'driver';

  constructor(props) {
    super(props);

    this.who = this.props.route.params.who;
  }


  @action select = (direction) => {
    this.select_dialog = direction;
    this.sub_places = false;
    this.places = this.placeStore.items.filter(v => v.parent_id === null);
  };

  @action selectPlace = (place, index) => {
    if (!this.select_dialog) {
      return;
    }
    const children = this.placeStore.items.filter(v => v.parent_id === place.id);
    if (children.length === 0 || (this.sub_places && index === 0)) {
      this[this.select_dialog] = place;
      this.select_dialog = null;
    } else {
      this.places = [place, ...children.filter(v => {
        if (this.select_dialog === 'from' && this.to) {
          return v.id !== this.to.id;
        }
        if (this.select_dialog === 'to' && this.from) {
          return v.id !== this.from.id;
        }
        return true;
      })];
      this.sub_places = true;
    }
  };

  @action setValue = (name, value) => this[name] = value;

  @action search = () => {
    if (this.loading || !this.from || !this.to) {
      return;
    }
    this.loading = true;
    requester.get('/trip', {
      from_id: this.from.id,
      to_id: this.to.id,
    }).then(({data}) => {
      if (data.result === 'success') {
        this.tripStore.setItems(data.payload.data);
      }
    }).catch(e => {
      e = getCustomError(e);
      console.log(e.toSnapshot());
    }).finally(() => {
      this.loading = false;
    });
  };

  @action fetchLastItems = () => {
    if (this.loading) return;
    this.loading = true;
    requester.get('/trip', {
      _order_by: 'datetime',
      _sorting: 'desc',
      who: this.who,
    }).then(({data}) => {
      if (data.result === 'success') {
        this.tripStore.setItems(data.payload.data);
      }
    }).catch(e => {
      e = getCustomError(e);
      console.log(e.toSnapshot());
    }).finally(() => {
      this.loading = false;
    });
  };

  componentDidMount(): void {
    this.fetchLastItems();
  }

  @action setWho = (s) => {
    if (this.loading || this.who === s) return;
    this.who = s;
    this.fetchLastItems();
  };

  render() {
    const {t} = this.props;

    return (
      <>
        <Portal>
          <Dialog
            visible={this.select_dialog !== null}
            onDismiss={() => this.select_dialog = null}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.selectPlace(item, index)} style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item.title}</Text>
                  </TouchableOpacity>
                )}
                data={this.places}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <FlatList
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item, index}) => (
            <TripItemComponent item={item} key={index}/>
          )}
          data={(this.to && this.from ?
            Array.from(this.tripStore.items.values()).filter(v => v.from_id === this.from.id && v.to_id === this.to.id && v.datetime > moment().subtract(5, 'hours')).sort((a, b) => a.datetime - b.datetime) :
            Array.from(this.tripStore.items.values()).filter(v => v.datetime > moment().subtract(5, 'hours')).sort((a, b) => a.datetime - b.datetime)).filter(v => v.who === this.who)}
          ListHeaderComponent={(
            <>
              <Card style={{margin: size(4)}} elevation={1}>
                <Card.Content>
                  <TouchableWithoutFeedback onPress={() => this.select('from')}>
                    <View style={{marginBottom: size(4)}}>
                      <TextInput
                        label={t('from')}
                        editable={false}
                        value={this.from ? this.from.title : ''}
                        mode={'outlined'}
                        dense={true}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                  <TouchableWithoutFeedback onPress={() => this.select('to')}>
                    <View>
                      <TextInput
                        label={t('to')}
                        editable={false}
                        value={this.to ? this.to.title : ''}
                        mode={'outlined'}
                        dense={true}
                      />
                    </View>
                  </TouchableWithoutFeedback>
                  <RadioButtons
                    style={{marginTop: size(2)}}
                    options={[
                      {value: 'driver', label: t(`i_need.driver`)},
                      {value: 'passenger', label: t(`i_need.passenger`)},
                    ]}
                    value={this.who}
                    onChange={value => this.setWho(value)}/>
                  <Button
                    icon={'search'}
                    loading={this.loading}
                    mode={'contained'}
                    style={{marginTop: size(2)}}
                    onPress={() => this.search()}
                  >{t('submit')}</Button>
                </Card.Content>
              </Card>
            </>
          )}
          ListEmptyComponent={(
            <Card elevation={2} style={{margin: size(4), marginTop: 0}}>
              <Card.Content alignItems={'center'}>
                {this.loading ? (
                  <ActivityIndicator/>
                ) : (
                  <>
                    <Image source={tripImage} style={{marginVertical: size(7), height: size(30), width: size(30)}}
                           resizeMode={'contain'}/>
                    <Text style={{textAlign: 'center', color: Colors.grey500}}>{t('list_empty')}</Text>
                  </>
                )}
              </Card.Content>
            </Card>
          )}/>
      </>
    );
  }
}

export default Find;

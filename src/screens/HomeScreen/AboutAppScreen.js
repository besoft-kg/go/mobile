import React from 'react';
import BaseComponent from '../../components/BaseComponent';
import ScreenWrapper from '../../ui/ScreenWrapper';
import {Card, Colors, Subheading, Text} from 'react-native-paper';
import {withTranslation} from 'react-i18next';
import {APP_VERSION_NAME} from '../../utils/settings';
import {size} from '../../utils';

@withTranslation()
class AboutAppScreen extends BaseComponent {
  render() {
    const {t} = this.props;

    return (
      <>
        <ScreenWrapper scroll>
          <Card elevation={0} style={{margin: size(4)}}>
            <Card.Content>
              <Text>{t('app.description')}</Text>
            </Card.Content>
          </Card>
          <Card elevation={0} style={{margin: size(4), marginTop: 0}}>
            <Card.Content>
              <Subheading>{t('our_contacts')}</Subheading>
              <Text style={{color: Colors.grey700}}>+996 700 237 638 (WhatsApp)</Text>
              <Text>
                <Text style={{color: Colors.grey700}}>Instagram</Text>: @bego.kg
              </Text>
            </Card.Content>
          </Card>
          <Card elevation={0} style={{margin: size(4), marginTop: 0}}>
            <Card.Content>
              <Text>
                <Text style={{color: Colors.grey700}}>Платформа:</Text> Android
              </Text>
              <Text>
                <Text style={{color: Colors.grey700}}>Версия:</Text> {APP_VERSION_NAME}
              </Text>
            </Card.Content>
          </Card>
        </ScreenWrapper>
      </>
    );
  }
}

export default AboutAppScreen;

import React from 'react';
import {inject, observer} from 'mobx-react';
import BaseComponent from '../../components/BaseComponent';
import ScreenWrapper from '../../ui/ScreenWrapper';
import {Button, Card, Dialog, Divider, Portal, Text, TextInput} from 'react-native-paper';
import {TouchableWithoutFeedback, TouchableOpacity, View, FlatList} from 'react-native';
import {action, observable} from 'mobx';
import {withTranslation} from 'react-i18next';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {requester, size} from '../../utils';

@withTranslation('home') @inject('store') @observer
class AddOfferScreen extends BaseComponent {
  @observable select_dialog = null;
  @observable sub_places = false;
  @observable places = [];
  @observable free_seats_modal = false;
  @observable datetime_modal = false;
  @observable datetime_mode = 'date';
  @observable loading = false;

  @observable from = null;
  @observable to = null;
  @observable persons = '3';
  @observable note = null;
  @observable datetime = moment().add(2, 'days').hours(10).minutes(0).seconds(0);

  @action select = (direction) => {
    this.select_dialog = direction;
    this.sub_places = false;
    this.places = this.placeStore.items.filter(v => v.parent_id === null);
  };

  @action selectPlace = (place, index) => {
    if (!this.select_dialog) {
      return;
    }
    const children = this.placeStore.items.filter(v => v.parent_id === place.id);
    if (children.length === 0 || (this.sub_places && index === 0)) {
      this[this.select_dialog] = place;
      this.select_dialog = null;
    } else {
      this.places = [place, ...children.filter(v => {
        if (this.select_dialog === 'from' && this.to) {
          return v.id !== this.to.id;
        }
        if (this.select_dialog === 'to' && this.from) {
          return v.id !== this.from.id;
        }
        return true;
      })];
      this.sub_places = true;
    }
  };

  @action selectPersons = (count) => {
    this.persons = count;
    this.free_seats_modal = false;
  };

  @action setValue = (name, value) => this[name] = value;

  @action toggleFreeSeatsModal = (s) => this.free_seats_modal = s;
  @action toggleDatetimeModal = (s) => this.datetime_modal = s;

  @action setDatetime = (e, date) => {
    if (e.type !== 'dismissed') {
      this.datetime = moment(date);
    } else {
      this.datetime_modal = false;
      this.datetime_mode = 'date';
      return;
    }
    if (this.datetime_mode === 'time') {
      this.datetime_modal = false;
      this.datetime_mode = 'date';
    } else {
      this.datetime_mode = 'time';
    }
  };

  @action add = () => {
    if (this.loading || !this.from || !this.to) {
      return;
    }
    this.loading = true;
    requester.post('/offer', {
      from: this.from.id,
      to: this.to.id,
      persons: this.persons,
      datetime: this.datetime.unix(),
      note: this.note,
    }).then(({data}) => {
      if (data.result === 'success') {
        this.offerStore.setItem(data.payload);
        this.props.navigation.replace('Offer', {item_id: data.payload.id});
      }
    }).catch(e => {
      console.log(e.toSnapshot());
    }).finally(() => {
      this.loading = false;
    });
  };

  render() {
    const {t} = this.props;

    return (
      <>
        {this.datetime_modal && (
          <DateTimePicker
            value={this.datetime.toDate()}
            mode={this.datetime_mode}
            is24Hour={true}
            display="default"
            onChange={(e, date) => this.setDatetime(e, date)}
            minimumDate={moment().add(1, 'day').hours(0).minutes(0).seconds(0).toDate()}
            maximumDate={moment().add(1, 'month').hours(0).minutes(0).seconds(0).toDate()}
          />
        )}

        <Portal>
          <Dialog
            visible={this.select_dialog !== null}
            onDismiss={() => this.select_dialog = null}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.selectPlace(item, index)} style={{paddingVertical: 8}}
                                    key={index}>
                    <Text>{item.title}</Text>
                  </TouchableOpacity>
                )}
                data={this.places}/>
            </Dialog.Content>
          </Dialog>
          <Dialog
            visible={this.free_seats_modal}
            onDismiss={() => this.toggleFreeSeatsModal(false)}>
            <Dialog.Content>
              <FlatList
                ItemSeparatorComponent={() => <Divider/>}
                keyExtractor={({index}) => index}
                renderItem={({item, index}) => (
                  <TouchableOpacity onPress={() => this.selectPersons((index + 1).toString())}
                                    style={{paddingVertical: size(4)}} key={index}>
                    <Text>{item}</Text>
                  </TouchableOpacity>
                )}
                data={['1', '2', '3', '4', '5', '6', '7', '8', '9']}/>
            </Dialog.Content>
          </Dialog>
        </Portal>

        <ScreenWrapper scroll>
          <Card style={{margin: size(4)}} elevation={1}>
            <Card.Content>
              <TouchableWithoutFeedback onPress={() => this.select('from')}>
                <View style={{marginBottom: size(4)}}>
                  <TextInput
                    label={t('add_offer.from')}
                    editable={false}
                    value={this.from ? this.from[`title_${this.store.language}`] : ''}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.select('to')}>
                <View>
                  <TextInput
                    label={t('add_offer.to')}
                    editable={false}
                    value={this.to ? this.to[`title_${this.store.language}`] : ''}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
            </Card.Content>
          </Card>
          <Card style={{margin: size(4), marginTop: 0}} elevation={1}>
            <Card.Content>
              <TouchableWithoutFeedback onPress={() => this.toggleFreeSeatsModal(true)}>
                <View style={{marginBottom: size(4)}}>
                  <TextInput
                    label={t('add_offer.free_seats')}
                    editable={false}
                    value={this.persons}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.toggleDatetimeModal(true)}>
                <View>
                  <TextInput
                    label={t('add_offer.datetime')}
                    editable={false}
                    value={this.datetime.calendar()}
                    mode={'outlined'}
                    dense={true}
                  />
                </View>
              </TouchableWithoutFeedback>
            </Card.Content>
          </Card>
          <Card style={{margin: size(4), marginTop: 0}} elevation={1}>
            <Card.Content>
              <TextInput
                label={t('add_offer.note')}
                placeholder={t('add_offer.note_placeholder')}
                value={this.note}
                onChangeText={s => this.setValue('note', s)}
                dense={true}
                numberOfLines={3}
                multiline={true}
              />
            </Card.Content>
          </Card>
          <Button
            loading={this.loading}
            mode={'contained'}
            onPress={() => this.add()}
            style={{marginHorizontal: size(4), marginBottom: size(4)}}
          >{t('add_offer.submit')}</Button>
        </ScreenWrapper>
      </>
    );
  }
}

export default AddOfferScreen;

import 'react-native-gesture-handler';
import React, {Suspense} from 'react';
import {Provider} from 'mobx-react';
import App from './App';
import RootStore from './stores/RootStore';
import {Provider as PaperProvider, DefaultTheme, Colors, ActivityIndicator} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {I18nextProvider} from 'react-i18next';
import i18n from './utils/i18n';
import {View} from 'react-native';
import {navigationRef} from './utils/RootNavigator';
import {OverflowMenuProvider} from 'react-navigation-header-buttons';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.green700,
    accent: Colors.green700,
  },
};

const Root = () => {
  return (
    <PaperProvider settings={{
      icon: props => <Icon {...props} />,
    }} theme={theme}>
      <Suspense fallback={(
        <View flex={1} justifyContent={'center'} alignItems={'center'}>
          <ActivityIndicator/>
        </View>
      )}>
        <I18nextProvider i18n={i18n}>
          <NavigationContainer ref={navigationRef}>
            <Provider store={RootStore}>
              <OverflowMenuProvider>
                <App/>
              </OverflowMenuProvider>
            </Provider>
          </NavigationContainer>
        </I18nextProvider>
      </Suspense>
    </PaperProvider>
  );
};

export default Root;

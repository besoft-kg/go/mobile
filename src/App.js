import React from 'react';
import {
  SafeAreaView, Linking,
  View, ScrollView,
} from 'react-native';
import {inject, observer} from 'mobx-react';
import 'mobx-react/batchingForReactNative';
import BaseComponent from './components/BaseComponent';
import WelcomeScreen from './screens/WelcomeScreen';
import {ActivityIndicator, Button, Caption, Card, Modal, Portal, Subheading} from 'react-native-paper';
import storage from './utils/storage';
import {APP_VERSION_CODE, initialize} from './utils/settings';
import {requester, size} from './utils';
import 'moment/locale/ru';
import 'moment/locale/ky';
import MainScreen from './screens/MainScreen';
import {action, observable} from 'mobx';
import {withTranslation} from 'react-i18next';

@withTranslation()
@inject('store')
@observer
class App extends BaseComponent {
  @observable last_version = null;

  constructor(props) {
    super(props);

    Promise.all([
      storage.get('language', null),
      storage.get('token', null),
      storage.get('user', null),
      storage.get('places', null),
      storage.get('fcm_token', null),
    ]).then(values => {
      this.rootStore.setDataFromStorage(values);
      initialize(this.store);

      requester.get('/place').then(({data}) => {
        this.placeStore.setItems(data.payload);
      }).catch((e) => {
        console.log(e);
      });

      this.checkLastVersion();

      if (this.rootStore.appStore.authenticated) {
        requester.get('/me').then(({data}) => {
          this.store.setUser(data.payload);
        }).catch(e => {
          console.log(e);
        });

        this.rootStore.tripStore.fetchItems();
      }
    });
  }

  @action checkLastVersion = () => {
    requester.get('/version/last', {platform: 'android'}).then(({data}) => {
      this.last_version = data.payload;
    }).catch((e) => {
      console.log(e);
    });
  };

  updateApp = () => {
    Linking.openURL(this.last_version.link);
  };

  render() {
    if (!this.store.app_is_ready) {
      return (
        <>
          <View flex={1} justifyContent={'center'} alignItems={'center'}>
            <ActivityIndicator/>
          </View>
        </>
      );
    }

    const {t} = this.props;

    return (
      <>
        <Portal>
          <Modal dismissable={false} contentContainerStyle={{padding: size(4)}}
                 visible={this.last_version && this.last_version.version_code > APP_VERSION_CODE}>
            <ScrollView>
              <Card>
                <Card.Content>
                  <Subheading style={{textAlign: 'center'}}>{t('new_version.title')}</Subheading>
                  <Caption>{t('new_version.description')}</Caption>
                  <Button onPress={() => this.updateApp()} style={{marginTop: size(4)}}
                          mode={'contained'}>{t('new_version.update')}</Button>
                </Card.Content>
              </Card>
            </ScrollView>
          </Modal>
        </Portal>

        {!this.store.language || !this.store.authenticated ? (
          <SafeAreaView flex={1}>
            <WelcomeScreen/>
          </SafeAreaView>
        ) : (
          <MainScreen/>
        )}
      </>
    );
  }
}

export default App;

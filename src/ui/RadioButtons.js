import {Text, TouchableOpacity, View} from 'react-native';
import {RadioButton, Colors, withTheme} from 'react-native-paper';
import React from 'react';

@withTheme
class RadioButtons extends React.Component {
  static defaultProps = {
    style: {},
  };

  render() {
    const {options, value, onChange, style} = this.props;

    return (
      <View style={style}>
        {options.map((v, k) => (
          <TouchableOpacity
            key={k}
            style={{alignItems: 'center', flexDirection: 'row'}}
            onPress={() => onChange(v.value)}>
            <RadioButton
              status={v.value === value ? 'checked' : 'unchecked'}
              onPress={() => onChange(v.value)}
              value={v.value}/>
            <Text style={{color: v.value === value ? this.props.theme.colors.accent : Colors.black}}>{v.label}</Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  }
}

export default RadioButtons;

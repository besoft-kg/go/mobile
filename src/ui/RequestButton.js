import React from 'react';
import BaseComponent from '../components/BaseComponent';
import {Button, Colors} from 'react-native-paper';
import {Alert, Linking} from 'react-native';
import {getCustomError, requester, size} from '../utils';
import {Observer} from 'mobx-react';
import RootNavigator from '../utils/RootNavigator';
import {withTranslation} from 'react-i18next';

@withTranslation('requests')
class RequestButton extends BaseComponent {

  sendRequest = () => {
    const {item, gItem, userType} = this.props;

    item.setValue('_loading', true);
    requester.post('/request', {
      offer: userType === 'passenger' ? item.id : gItem.id,
      trip: userType === 'passenger' ? gItem.id : item.id,
    }).then(({data}) => {
      gItem.setRequest(data.payload);
    }).catch(e => {
      e = getCustomError(e);
      console.log(e.toSnapshot());
    }).finally(() => {
      item.setValue('_loading', false);
    });
  };

  cancelRequest = () => {
    const {item, gItem} = this.props;

    item.setValue('_loading', true);
    requester.put('/request', {
      id: item.request.id,
      status: 'cancelled',
    }).then(({data}) => {
      gItem.setRequest(data.payload);
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      item.setValue('_loading', false);
    });
  };

  rejectRequest = () => {
    const {item, gItem} = this.props;

    item.setValue('_loading', true);
    requester.put('/request', {
      id: item.request.id,
      status: 'rejected',
    }).then(({data}) => {
      gItem.setRequest(data.payload);
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      item.setValue('_loading', false);
    });
  };

  acceptRequest = () => {
    const {item, gItem, userType} = this.props;

    item.setValue('_loading', true);
    requester.put('/request', {
      id: item.request.id,
      status: 'accepted',
    }).then(({data}) => {
      gItem.setRequest(data.payload);
      if (userType === 'passenger') {
        RootNavigator.replace('Trip', {item_id: gItem.id});
      }
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      item.setValue('_loading', false);
    });
  };

  confirmCancelRequest = () => {
    const {item, t} = this.props;

    if (item._loading) {
      return;
    }
    Alert.alert(
      t('alert.confirm_cancel.title'),
      t('alert.confirm_cancel.message'),
      [
        {
          text: t('alert.confirm_cancel.cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => this.cancelRequest(),
        },
      ],
    );
  };

  confirmRejectRequest = () => {
    const {item, t} = this.props;

    if (item._loading) {
      return;
    }
    Alert.alert(
      t('alert.confirm_reject.title'),
      t('alert.confirm_reject.message'),
      [
        {
          text: t('alert.confirm_reject.cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => this.rejectRequest(),
        },
      ],
    );
  };

  confirmSendRequest = () => {
    const {item, t} = this.props;

    if (item._loading) {
      return;
    }
    Alert.alert(
      t('alert.confirm_send.title'),
      t('alert.confirm_send.message', {full_name: item.user.full_name}),
      [
        {
          text: t('alert.confirm_send.cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => this.sendRequest(),
        },
      ],
    );
  };

  confirmAcceptRequest = () => {
    const {item, t} = this.props;

    if (item._loading) {
      return;
    }
    Alert.alert(
      t('alert.confirm_accept.title'),
      t('alert.confirm_accept.message', {full_name: item.user.full_name}),
      [
        {
          text: t('alert.confirm_accept.cancel'),
          onPress: () => {},
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => this.acceptRequest(),
        },
      ],
    );
  };

  call = () => {
    if (this.props.item.request) {
      Linking.openURL(`tel:+${this.props.item.request[this.props.userType === 'driver' ? 'passenger' : 'driver'].phone_number}`);
    }
  };

  render() {
    return (
      <Observer>
        {() => {
          const {item, userType, gItem, t} = this.props;
          const {request} = item;

          if (!request) {
            if (gItem.is_past) return null;
            return (
              <Button
                disabled={item._loading}
                loading={item._loading}
                icon={'help-outline'}
                onPress={() => this.confirmSendRequest()}
                mode={'outlined'}
                style={{marginTop: size(4)}}
              >{t('send')}</Button>
            );
          }

          switch (request.status) {
            case 'pending':
              if (request.sender === userType) {
                return (
                  <Button
                    disabled={item._loading || gItem.is_past}
                    loading={item._loading}
                    color={Colors.red700}
                    onPress={() => this.confirmCancelRequest()}
                    mode={'outlined'}
                    icon={'cancel'}
                    style={{marginTop: size(4), borderColor: Colors.red100}}
                  >{t('cancel')}</Button>
                );
              } else {
                return (
                  <>
                    <Button
                      disabled={item._loading || gItem.is_past}
                      loading={item._loading}
                      color={Colors.blue700}
                      icon={'check'}
                      onPress={() => this.confirmAcceptRequest()}
                      mode={'contained'}
                      style={{marginTop: size(4), borderColor: Colors.blue100}}
                    >{t('accept')}</Button>
                    <Button
                      disabled={item._loading || gItem.is_past}
                      loading={item._loading}
                      color={Colors.red700}
                      onPress={() => this.confirmRejectRequest()}
                      mode={'outlined'}
                      icon={'cancel'}
                      style={{marginTop: size(4), borderColor: Colors.red100}}
                    >{t('reject')}</Button>
                  </>
                );
              }
              break;

            case 'cancelled':
              if (request.sender === userType) {
                return (
                  <Button
                    disabled={true}
                    loading={item._loading}
                    mode={'contained'}
                    icon={'cancel'}
                    style={{marginTop: size(4)}}
                  >{t('cancelled.you')}</Button>
                );
              } else {
                return (
                  <Button
                    icon={'cancel'}
                    disabled={true}
                    loading={item._loading}
                    mode={'contained'}
                    style={{marginTop: size(4)}}
                  >{t('cancelled.he')}</Button>
                );
              }
              break;

            case 'rejected':
              if (request.sender !== userType) {
                return (
                  <Button
                    disabled={true}
                    loading={item._loading}
                    mode={'contained'}
                    icon={'cancel'}
                    style={{marginTop: size(4)}}
                  >{t('rejected.you')}</Button>
                );
              } else {
                return (
                  <Button
                    icon={'cancel'}
                    disabled={true}
                    loading={item._loading}
                    mode={'contained'}
                    style={{marginTop: size(4)}}
                  >{t('rejected.he')}</Button>
                );
              }
              break;

            case 'accepted':
              return (
                <>
                  <Button
                    color={Colors.green700}
                    icon={'phone'}
                    onPress={() => this.call()}
                    mode={'contained'}
                    style={{marginTop: size(4)}}
                  >{t('call')}</Button>
                  <Button
                    disabled={item._loading || gItem.is_past}
                    loading={item._loading}
                    color={Colors.red700}
                    onPress={() => userType === request.sender ? this.confirmCancelRequest() : this.confirmRejectRequest()}
                    mode={'outlined'}
                    icon={'cancel'}
                    style={{marginTop: size(4), borderColor: Colors.red100}}
                  >{t('reject_accepted')}</Button>
                </>
              );
              break;
          }
        }}
      </Observer>
    );
  }
}

export default RequestButton;

import React from "react";
import {HeaderButton, HeaderButtons} from "react-navigation-header-buttons";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const MaterialHeaderButton = props => (
  <HeaderButton {...props} IconComponent={MaterialIcons} iconSize={23} color="#fff" />
);

export const MaterialHeaderButtons = props => {
  return (
    <HeaderButtons
      HeaderButtonComponent={MaterialHeaderButton}
      {...props}
    >
      {props.children}
    </HeaderButtons>
  );
};

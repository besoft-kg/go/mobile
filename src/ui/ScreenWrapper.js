import React from "react";
import {View, ScrollView} from "react-native";
import {size} from '../utils';

class ScreenWrapper extends React.Component {
  static defaultProps = {
    padding: null,
    scroll: false,
    scrollViewProps: {},
    style: {},
  };

  render() {
    return (
      <View style={{flex: 1, padding: this.props.padding ? size(4) : 0, ...this.props.style}}>
        {this.props.scroll ? (
          <ScrollView {...this.props.scrollViewProps}>
            {this.props.children}
          </ScrollView>
        ) : (
          <>
            {this.props.children}
          </>
        )}
      </View>
    );
  }
}

export default ScreenWrapper;

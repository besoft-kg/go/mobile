import {getRoot, getSnapshot, types as t} from 'mobx-state-tree';
import User from '../models/User';
import storage from '../utils/storage';
import {ToastAndroid, Platform, Alert} from 'react-native';
import CustomError from '../utils/CustomError';
import i18n from '../utils/i18n';
import moment from 'moment';
import {requester} from '../utils';
import {APP_VERSION_CODE} from '../utils/settings';

export default t
  .model('AppStore', {

    user: t.maybeNull(t.reference(User)),
    authenticating: t.boolean,
    checking: t.boolean,
    token: t.maybeNull(t.string),
    language: t.maybeNull(t.enumeration(['ru', 'ky'])),
    app_is_ready: t.boolean,
    fcm_token: t.maybeNull(t.string),
    global_who: t.optional(t.enumeration(['driver', 'passenger']), 'driver'),

    error: t.maybeNull(t.custom({
      name: 'CustomError',
      fromSnapshot: (snapshot => new CustomError().fromSnapshot(snapshot)),
      toSnapshot: (value) => value.toSnapshot(),
      isTargetType: (value) => value instanceof CustomError,
      getValidationMessage(value: string): string {
        return `'${value}' doesn't look like a CustomError object`;
      },
    })),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const setFcmToken = (token, save_to_storage = true) => {
      self.fcm_token = token;
      if (save_to_storage) {
        storage.set('fcm_token', token);
      }
      if (self.token) {
        requester.put('/me/session', {
          fcm_token: token,
          version_code: APP_VERSION_CODE,
        }).then(() => {}).catch(e => console.log(e));
      }
    };

    const setUser = (data) => {
      getRoot(self).userStore.setItem(data);
    };

    const signIn = (data) => {
      const root = getRoot(self);

      storage.set('token', data.token);

      root.userStore.setItem(data.user);

      self.user = root.userStore.items.get(data.user.id);
      self.token = data.token;

      storage.set('user', getSnapshot(self.user));
    };
    function signOut() {
      const root = getRoot(self);

      storage.remove('token');
      storage.remove('user');
      self.user = null;
      self.token = null;

      root.tripStore.clear();
    }

    const setLanguage = (lng, save_to_storage = true) => {
      self.language = lng;
      moment.locale(lng);
      i18n.changeLanguage(lng);
      if (save_to_storage) {
        storage.set('language', lng);
      }
    };

    const clearError = () => {
      self.error = null;
    };
    const failed = (error) => {
      self.error = error;
    };

    const showMessage = (content, variant = 'default') => {
      if (Platform.OS !== 'ios') ToastAndroid.show(content, ToastAndroid.LONG);
      else Alert.alert(content);
    };

    const showError = (content) => {
      showMessage(content, 'error');
    };
    const showWarning = (content) => {
      showMessage(content, 'warning');
    };
    const showSuccess = (content) => {
      showMessage(content, 'success');
    };
    const showInfo = (content) => {
      showMessage(content, 'info');
    };

    return {
      showError,
      showInfo,
      showWarning,
      showSuccess,
      clearError,
      failed,
      setLanguage,
      signOut,
      setValue,
      signIn,
      setFcmToken,
      setUser,
    };

  })
  .views(self => ({

    get error_occurred() {
      return self.error !== null;
    },

    get authenticated() {
      return self.user !== null;
    },

  }));

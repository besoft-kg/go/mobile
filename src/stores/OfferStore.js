import {getRoot, types as t} from 'mobx-state-tree';
import {getCustomError, requester} from '../utils';

export default t
  .model('OfferStore', {

    items: t.map(Offer),
    is_fetching: t.optional(t.boolean, false),

  })
  .actions(self => {

    function setValue(name, value) {
      self[name] = value;
    }

    function setItem(data) {
      const root = getRoot(self);

      if (data.trips) {
        root.tripStore.setItems(data.trips);
        data.trips.map(trip => root.userStore.setItem(trip.user));
      }

      const item = Offer.create({
        ...data,
        user: data.user_id,
        from: data.from_id,
        to: data.to_id,
        datetime: new Date(data.datetime),
        created_at: new Date(data.created_at),
      });

      if (self.items.has(item.id)) self.items[item.id] = item;
      else self.items.put(item);
    }

    function setItems(data) {
      const root = getRoot(self);

      root.requestStore.setItems([]);

      data.map(v => {
        if (v.trips) {
          root.tripStore.setItems(v.trips);
          v.trips.map(trip => root.userStore.setItem(trip.user));
        }
        if (v.requests) {
          root.requestStore.setItems(v.requests);
        }

        self.items.put(Offer.create({
          ...v,
          user: v.user_id,
          from: v.from_id,
          to: v.to_id,
          datetime: new Date(v.datetime),
          created_at: new Date(v.created_at),
        }));
      });
    }

    function fetchItems() {
      if (self.is_fetching) {
        return;
      }
      self.items.clear();
      setValue('is_fetching', true);
      requester.get('/offer', {
        _order_by: 'datetime',
        _sorting: 'asc',
      }).then(({data}) => {
        if (data.result === 'success') {
          self.setItems(data.payload.data);
        }
      }).catch(e => {
        e = getCustomError(e);
        console.log(e.toSnapshot());
      }).finally(() => {
        self.setValue('is_fetching', false);
      });
    }

    function fetchItem(id) {
      if (self.is_fetching) {
        return;
      }
      setValue('is_fetching', true);
      requester.get('/offer', {
        _order_by: 'datetime',
        _sorting: 'asc',
        id,
      }).then(({data}) => {
        if (data.result === 'success') {
          self.setItem(data.payload);
        }
      }).catch(e => {
        e = getCustomError(e);
        console.log(e.toSnapshot());
      }).finally(() => {
        self.setValue('is_fetching', false);
      });
    }

    function clear() {
      self.items.clear();
    }

    return {
      setValue,
      setItem,
      setItems,
      fetchItems,
      fetchItem,
      clear,
    };

  })
  .views(self => ({}));

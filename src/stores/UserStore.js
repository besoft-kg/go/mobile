import {types as t} from 'mobx-state-tree';
import User from '../models/User';

export default t
  .model('UserStore', {

    items: t.map(User),

  })
  .actions(self => ({

    setValue(name, value) {
      self[name] = value;
    },

    setItems(p) {
      p.map(v => {
        self.items.set(v.id, {
          ...v,
          last_action: new Date(v.last_action),
          created_at: new Date(v.created_at),
        });
      });
    },

    setItem(v) {
      self.items.set(v.id, {
        ...v,
        last_action: new Date(v.last_action),
        created_at: new Date(v.created_at),
      });
    },

  }))
  .views(self => ({}));

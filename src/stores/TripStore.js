import {destroy, getRoot, types as t} from 'mobx-state-tree';
import Trip from '../models/Trip';
import {getCustomError, requester} from '../utils';

export default t
  .model('TripStore', {

    items: t.map(Trip),
    is_fetching: t.optional(t.boolean, false),

  })
  .actions(self => {

    function setValue(name, value) {
      self[name] = value;
    }

    function setItem(data) {
      self.items.set(data.id, Trip.create({
        ...data,
        user: data.user_id,
        from: data.from_id,
        to: data.to_id,
        datetime: new Date(data.datetime),
        created_at: new Date(data.created_at),
      }));
    }

    function setItems(data) {
      data.map(v => {
        if (v.user) {
          getRoot(self).userStore.setItem(v.user);
        }

        self.items.set(v.id, Trip.create({
          ...v,
          user: v.user_id,
          from: v.from_id,
          to: v.to_id,
          datetime: new Date(v.datetime),
          created_at: new Date(v.created_at),
        }));
      });
    }

    function fetchItems() {
      if (self.is_fetching) return;
      self.items.clear();
      setValue('is_fetching', true);
      requester.get('/trip', {
        _order_by: 'datetime',
        _sorting: 'asc',
        user_id: getRoot(self).appStore.user.id,
      }).then(({data}) => {
        if (data.result === 'success') {
          self.setItems(data.payload.data);
        }
      }).catch(e => {
        e = getCustomError(e);
        console.log(e.toSnapshot());
      }).finally(() => {
        self.setValue('is_fetching', false);
      });
    }

    function clear() {
      self.items.clear();
    }

    function destroyItem(item) {
      destroy(item);
    }

    return {
      setValue,
      setItem,
      setItems,
      fetchItems,
      clear,
      destroyItem,
    };

  })
  .views(self => ({}));

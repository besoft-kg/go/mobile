import React from 'react';

class BaseComponent extends React.Component {
  constructor(props) {
    super(props);

    this.rootStore = props.store;

    if (this.rootStore) {
      this.store = this.rootStore.appStore;
      this.placeStore = this.rootStore.placeStore;
      this.tripStore = this.rootStore.tripStore;
      this.userStore = this.rootStore.userStore;
    }
  }
}

export default BaseComponent;

import React from 'react';
import BaseComponent from '../BaseComponent';
import {Image, StatusBar, Text, View} from 'react-native';
import {ActivityIndicator, Button, Card, Colors, TextInput, Title} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import {getExampleNumber} from 'libphonenumber-js';
import phoneNumberExamples from 'libphonenumber-js/examples.mobile.json';
import {requester, size} from '../../utils';
import {action, observable} from 'mobx';
import PhoneNumber from '../../utils/PhoneNumber';
import {withTranslation} from 'react-i18next';
import logo from '../../assets/logo-white-512х277.png';

@withTranslation('welcome') @inject('store') @observer
class AuthPageComponent extends BaseComponent {
  phone_number_ref = React.createRef();

  @observable loading = false;
  @observable phone_E164_format = '';
  @observable valid_phone_number = false;
  @observable phone_number = '';

  constructor(props) {
    super(props);

    this.phone_number_placeholder = getExampleNumber('KG', phoneNumberExamples).formatNational();
    this.phone = new PhoneNumber();
  }

  @action setPhoneNumber(s) {
    this.phone.parseFromString(s);
    this.valid_phone_number = this.phone.isValid();
    this.phone_E164_format = this.phone.getE164Format();
    this.phone_number = s;
  }

  focus = () => {
    this.phone_number_ref.focus();
  };

  checkPhone = () => {
    if (this.loading) {
      return;
    }

    const {t} = this.props;

    if (this.phone_number.length === 0) {
      this.store.showError(t('toast.error.phone_is_empty'));
      return;
    } else if (!this.valid_phone_number) {
      this.store.showError(t('toast.error.invalid_phone'));
      return;
    }

    this.loading = true;
    requester.post('/auth/phone', {phone_number: this.phone_E164_format.slice(1)}).then(({data}) => {
      switch (data.result) {
        case 'exists_code_is_sent':
          this.props.onChange('status', 'exists');
          break;
        case 'exists_code_was_sent':
          this.props.onChange('status', 'exists');
          break;
        case 'not_exists_code_was_sent':
          this.props.onChange('status', 'not_exists');
          break;
        case 'not_exists_code_is_sent':
          this.props.onChange('status', 'not_exists');
          break;
      }
      //this.store.showSuccess(t('toast.success.we_sent_code'));
      this.props.onChange('phone_number', this.phone_E164_format.slice(1));
      setTimeout(() => {
        this.props.viewPagerRef.current.setPage(2);
      }, 100);
    }).catch(error => {
      console.log(error);
      this.props.onChange('status', '');
    }).finally(() => {
      this.loading = false;
    });
  };

  render() {
    const {t} = this.props;

    return (
      <View style={{
        flex: 1,
        backgroundColor: Colors.teal700,
        paddingTop: StatusBar.currentHeight,
        paddingHorizontal: size(4),
        paddingBottom: size(4),
        justifyContent: 'space-between',
        alignItems: 'center',
      }} key="2">
        <View alignItems={'center'}>
          <Text style={{
            textShadowRadius: 3,
            textShadowColor: 'white',
            color: '#fff',
            marginTop: size(8),
            marginBottom: size(4),
          }}>Besoft</Text>
          <Image resizeMode={'contain'} style={{height: size(10)}} source={logo}/>
        </View>
        <View style={{width: '100%'}}>
          <View alignItems={'center'} style={{marginBottom: size(4)}}>
            <Title style={{color: '#fff', paddingTop: size(8)}}>Авторизация</Title>
          </View>
          <Card style={{marginBottom: size(4)}}>
            <Card.Content>
              {this.loading ? (
                <View flex={1} style={{marginVertical: size(26)}}>
                  <ActivityIndicator/>
                </View>
              ) : (
                <>
                  <Text style={{marginBottom: size(4), color: Colors.grey600}}>{t('please_authorize')}</Text>
                  <TextInput
                    ref={r => this.phone_number_ref = r}
                    label={t('phone_number_input_label')}
                    autoCompleteType={'tel'}
                    value={this.phone_number}
                    keyboardType={'phone-pad'}
                    returnKeyType={'next'}
                    mode={'outlined'}
                    dense={true}
                    disabled={this.props.status !== ''}
                    dataDetectorTypes={'phoneNumber'}
                    autoFocus={true}
                    blurOnSubmit={true}
                    onSubmitEditing={() => this.checkPhone()}
                    textContentType={'telephoneNumber'}
                    placeholder={this.phone_number_placeholder}
                    onChangeText={text => this.setPhoneNumber(text)}
                  />
                </>
              )}
            </Card.Content>
          </Card>
          <Button
            dark={false}
            contentStyle={{backgroundColor: Colors.white}}
            disabled={this.loading}
            onPress={() => this.checkPhone()}
            mode={'contained'}
            style={{width: '100%'}}>
            ОК
          </Button>
        </View>
      </View>
    );
  }
}

export default AuthPageComponent;

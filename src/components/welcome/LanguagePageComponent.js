import React from 'react';
import BaseComponent from '../BaseComponent';
import {StatusBar, Text, View, Image} from 'react-native';
import {Button, Card, Colors, withTheme} from 'react-native-paper';
import RadioButtons from '../../ui/RadioButtons';
import {inject, observer} from 'mobx-react';
import logo from '../../assets/logo-white-512х277.png';
import {size} from '../../utils';

const language_options = [
  {value: 'ky', label: 'Кыргызча'},
  {value: 'ru', label: 'Русский'},
];

@withTheme @inject('store') @observer
class LanguagePageComponent extends BaseComponent {
  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: this.props.theme.colors.accent,
        paddingTop: StatusBar.currentHeight,
        paddingHorizontal: size(4),
        paddingBottom: size(4),
        justifyContent: 'space-between',
        alignItems: 'center',
      }} key="1">
        <View/>
        <View alignItems={'center'}>
          <Text style={{fontSize: size(8), textShadowRadius: 3, textShadowColor: 'white', color: '#fff', marginBottom: 16}}>Besoft</Text>
          <Image resizeMode={'contain'} style={{height: size(50)}} source={logo}/>
          <Text style={{fontSize: size(10), textShadowRadius: 3, textShadowColor: 'white', color: '#fff', marginTop: 16}}>С вами по пути!</Text>
        </View>
        <View alignItems={'center'}>
          <Text style={{fontSize: size(10), color: '#fff', paddingTop: 16}}>Добро пожаловать!</Text>
          <Text style={{fontSize: size(10), color: '#fff'}}>Кош келиңиз!</Text>
        </View>
        <View>
          <Text style={{textAlign: 'center', color: '#fff'}}>Пожалуйста, выберите язык чтобы продолжить</Text>
          <Text style={{textAlign: 'center', color: '#fff', paddingTop: size(4)}}>Сураныч, улантуу үчүн тил тандаңыз</Text>
        </View>
        <View style={{width: '100%'}}>
          <Card style={{marginBottom: size(4)}}>
            <Card.Content>
              <RadioButtons
                options={language_options}
                value={this.store.language}
                onChange={value => this.store.setLanguage(value)}/>
            </Card.Content>
          </Card>
          <Button
            dark={false}
            contentStyle={{backgroundColor: Colors.white}}
            disabled={!this.store.language}
            onPress={() => this.props.viewPagerRef.current.setPage(1)}
            mode={'contained'}
            style={{width: '100%'}}>
            ОК
          </Button>
        </View>
      </View>
    );
  }
}

export default LanguagePageComponent;

import React from 'react';
import BaseComponent from '../BaseComponent';
import {StatusBar, Text, View, Platform, Image} from 'react-native';
import {ActivityIndicator, Button, Card, Colors, TextInput, Title} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import {action, computed, observable} from 'mobx';
import {getCustomError, requester, size} from '../../utils';
import {APP_VERSION_CODE} from '../../utils/settings';
import logo from '../../assets/logo-white-512х277.png';
import {withTranslation} from 'react-i18next';

@withTranslation('welcome')
@inject('store') @observer
class LoginPageComponent extends BaseComponent {
  verification_code_ref = React.createRef();

  @observable verification_code = '';
  @observable loading = false;

  @action setValue = (name, value) => this[name] = value;

  login = () => {
    const {t} = this.props;

    if (this.button_is_disabled) {
      if (this.verification_code.length !== 6) {
        this.verification_code_ref.focus();
        this.store.showError(t('toast.error.incorrect_code'));
      }
      return;
    }

    if (this.loading) {
      return;
    }

    this.loading = true;
    requester.post('/auth/phone/login', {
      phone_number: this.props.phoneNumber,
      code: this.verification_code,
      language: this.store.language,
      platform: Platform.OS,
      fcm_token: this.store.fcm_token,
      version_code: APP_VERSION_CODE,
    }).then(({data}) => {
      this.store.signIn(data.payload);
    }).catch(error => {
      error = getCustomError(error);
      switch (error.result) {
        case 'code_is_incorrect':
          this.store.showError(t('toast.error.incorrect_code'));
          break;
        case 'code_is_invalid':
          this.store.showError(t('toast.error.invalid_code'));
          break;
      }
    }).finally(() => {
      this.loading = false;
    });
  };

  focus = () => {
    this.verification_code_ref.focus();
  };

  @computed get button_is_disabled() {
    if (this.loading) return true;
    return this.verification_code.length !== 6;
  }

  render() {
    const {t} = this.props;

    return (
      <View style={{
        flex: 1,
        backgroundColor: Colors.pink700,
        paddingTop: StatusBar.currentHeight,
        paddingHorizontal: size(4),
        paddingBottom: size(4),
        justifyContent: 'space-between',
        alignItems: 'center',
      }} key="3">
        <View alignItems={'center'}>
          <Text style={{textShadowRadius: 3, textShadowColor: 'white', color: '#fff', marginTop: size(8), marginBottom: size(4)}}>Besoft</Text>
          <Image resizeMode={'contain'} style={{height: size(10)}} source={logo}/>
        </View>
        <View style={{width: '100%'}}>
          <View alignItems={'center'} style={{marginBottom: size(4)}}>
            <Title style={{color: '#fff', paddingTop: size(8)}}>{t('sign_in')}</Title>
          </View>
          <Card style={{marginBottom: size(4)}}>
            <Card.Content>
              {this.loading ? (
                <View flex={1} style={{marginVertical: size(25)}}>
                  <ActivityIndicator/>
                </View>
              ) : (
                <>
                  <TextInput
                    ref={r => this.verification_code_ref = r}
                    label={t('code_input_label')}
                    value={this.verification_code}
                    mode={'outlined'}
                    dense={true}
                    keyboardType={'numeric'}
                    returnKeyType={'next'}
                    blurOnSubmit={true}
                    onSubmitEditing={() => this.login()}
                    autoFocus={true}
                    onChangeText={text => this.setValue('verification_code', text)}
                  />
                </>
              )}
            </Card.Content>
          </Card>
          <Button
            dark={false}
            contentStyle={{backgroundColor: Colors.white}}
            disabled={this.loading}
            onPress={() => this.login()}
            mode={'contained'}
            style={{width: '100%'}}>
            {t('sign_in_submit')}
          </Button>
        </View>
      </View>
    );
  }
}

export default LoginPageComponent;

import React from 'react';
import BaseComponent from '../BaseComponent';
import {StatusBar, Text, View, Platform, Image} from 'react-native';
import {ActivityIndicator, Button, Card, Colors, Divider, TextInput, Title} from 'react-native-paper';
import {inject, observer} from 'mobx-react';
import RadioButtons from '../../ui/RadioButtons';
import {action, computed, observable} from 'mobx';
import {requester, size} from '../../utils';
import {APP_VERSION_CODE} from '../../utils/settings';
import logo from '../../assets/logo-white-512х277.png';
import {withTranslation} from 'react-i18next';

@withTranslation('welcome')
@inject('store') @observer
class RegisterPageComponent extends BaseComponent {
  verification_code_ref = React.createRef();
  full_name_ref = React.createRef();

  @observable verification_code = '';
  @observable full_name = '';
  @observable gender = null;
  @observable type = null;

  @observable loading = false;

  @action setValue = (name, value) => this[name] = value;

  register = () => {
    const {t} = this.props;

    if (this.button_is_disabled) {
      if (this.verification_code.length !== 6) {
        this.verification_code_ref.focus();
        this.store.showError(t('toast.error.incorrect_code'));
      } else if (!this.full_name) {
        this.full_name_ref.focus();
      }
      return;
    }

    if (this.loading) {
      return;
    }

    this.loading = true;
    requester.post('/auth/phone/register', {
      phone_number: this.props.phoneNumber,
      code: this.verification_code,
      language: this.store.language,
      full_name: this.full_name,
      gender: this.gender,
      platform: Platform.OS,
      version_code: APP_VERSION_CODE,
      type: this.type,
      fcm_token: this.store.fcm_token,
    }).then(({data}) => {
      this.store.signIn(data.payload);
    }).catch(error => {
      switch (error.result) {
        case 'code_is_incorrect':
          this.store.showError(t('toast.error.incorrect_code'));
          break;
        case 'code_is_invalid':
          this.store.showError(t('toast.error.invalid_code'));
          break;
      }
      console.log(error.message);
    }).finally(() => {
      this.loading = false;
    });
  };

  focus = () => {
    this.verification_code_ref.focus();
  };

  @computed get button_is_disabled() {
    if (this.loading) {
      return true;
    }
    return this.verification_code.length !== 6 || !this.gender || !this.full_name;
  }

  render() {
    const {t} = this.props;

    return (
      <View style={{
        flex: 1,
        backgroundColor: Colors.pink700,
        paddingTop: StatusBar.currentHeight,
        paddingHorizontal: size(4),
        paddingBottom: size(4),
        justifyContent: 'space-between',
        alignItems: 'center',
      }} key="3">
        <View alignItems={'center'}>
          <Text style={{textShadowRadius: 3, textShadowColor: 'white', color: '#fff', marginTop: size(8), marginBottom: size(4)}}>Besoft</Text>
          <Image resizeMode={'contain'} style={{height: size(10)}} source={logo}/>
        </View>
        <View style={{width: '100%'}}>
          <View alignItems={'center'}>
            <Title style={{color: '#fff', paddingTop: size(8)}}>{t('sign_up')}</Title>
          </View>
          <Card style={{marginBottom: 8}}>
            <Card.Content>
              {this.loading ? (
                <View flex={1} style={{marginVertical: size(25)}}>
                  <ActivityIndicator/>
                </View>
              ) : (
                <>
                  <TextInput
                    ref={r => this.verification_code_ref = r}
                    label={t('code_input_label')}
                    value={this.verification_code}
                    mode={'outlined'}
                    dense={true}
                    keyboardType={'numeric'}
                    returnKeyType={'next'}
                    blurOnSubmit={true}
                    onSubmitEditing={() => this.full_name_ref.focus()}
                    autoFocus={true}
                    onChangeText={text => this.setValue('verification_code', text)}
                  />
                  <TextInput
                    ref={r => this.full_name_ref = r}
                    label={t('full_name_input_label')}
                    value={this.full_name}
                    mode={'outlined'}
                    dense={true}
                    onChangeText={text => this.setValue('full_name', text)}
                  />
                  <RadioButtons
                    style={{marginVertical: size(4)}}
                    options={[
                      {value: 'female', label: t('female_input_label')},
                      {value: 'male', label: t('male_input_label')},
                    ]}
                    value={this.gender}
                    onChange={value => this.setValue('gender', value)}/>
                </>
              )}
            </Card.Content>
          </Card>
          <Button
            dark={false}
            contentStyle={{backgroundColor: Colors.white}}
            disabled={this.loading}
            onPress={() => this.register()}
            mode={'contained'}
            style={{width: '100%'}}>
            {t('sign_up_submit')}
          </Button>
        </View>
      </View>
    );
  }
}

export default RegisterPageComponent;

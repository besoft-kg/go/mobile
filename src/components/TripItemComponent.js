import React from 'react';
import BaseComponent from './BaseComponent';
import {Button, Card, Colors, Subheading, Text, Title, withTheme} from 'react-native-paper';
import {Linking, View, Alert} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import {size} from '../utils';
import {inject, observer} from 'mobx-react';
import {withTranslation} from 'react-i18next';

@withTheme @withTranslation('trips')
@inject('store') @observer
class TripItemComponent extends BaseComponent {
  call = () => {
    const {item} = this.props;
    if (item.contacts[0].type === 'phone_number') {
      Linking.openURL(`tel:+${item.contacts[0].value}`);
    }
  };

  delete = () => {
    this.props.item.delete();
  };

  deleteConfirm = () => {
    Alert.alert(
      'Подтвердите',
      'Вы уверены?',
      [
        {
          text: 'Нет',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'Да', onPress: () => this.delete()},
      ],
      {cancelable: true},
    );
  };

  render() {
    const {item, t} = this.props;

    return (
      <Card style={{marginHorizontal: size(4), marginBottom: size(4)}}>
        <Card.Content>
          <Subheading style={{color: Colors.green600}}>
            {item.from.title} <Icon name={'keyboard-arrow-right'} size={12}/> {item.to.title}
          </Subheading>
          <Text style={{color: Colors.grey500}}>{t(`item.who.${item.who}`)}</Text>
          <Text style={{color: Colors.grey500}}>{t('item.author', {full_name: item.user.full_name})}</Text>
          <Text
            style={{color: Colors.grey500}}>{t('item.datetime', {datetime: moment(item.datetime).calendar()})}</Text>
          {item.note ? <Text style={{color: Colors.grey700}}>{item.note}</Text> : null}
          <View flexDirection={'row'} justifyContent={'space-between'} alignItems={'center'}>
            <Text style={{color: Colors.grey700}}>{item.persons_text}</Text>
            <Title style={{
              color: this.props.theme.colors.primary,
              fontWeight: 'bold',
            }}>{item.price ? `${item.price} сом` : t('item.negotiable')}</Title>
          </View>
          <Button icon={'call'} onPress={this.call} style={{marginTop: size(2)}} mode={'outlined'}>{t('item.call_button')}</Button>
          {this.store.user.id === item.user.id ? (
            <Button disabled={item._loading} icon={'delete'} onPress={this.deleteConfirm}
                    theme={{colors: {primary: Colors.red700}}}
                    style={{marginTop: size(2)}} mode={'outlined'}>{t('item.delete_button')}</Button>
          ) : null}
        </Card.Content>
      </Card>
    );
  }
}

export default TripItemComponent;

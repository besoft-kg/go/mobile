import {types as t} from 'mobx-state-tree';

export default t
  .model('User', {
    id: t.identifierNumber,
    phone_number: t.string,
    full_name: t.string,
    gender: t.enumeration(['male', 'female']),
    //contacts: t.maybeNull(t.map),
    abilities: t.maybeNull(t.map(t.string)),
    picture: t.maybeNull(t.string),
    bio: t.maybeNull(t.string),
    language: t.enumeration(['ru', 'ky']),
    last_action: t.Date,
    created_at: t.Date,
  })
  .views(self => ({
      get two_letter_name() {
          return (self.full_name.slice(0, 1) + self.full_name.slice(0, 1)).toUpperCase();
      },
  }))
  .actions(self => ({}));
